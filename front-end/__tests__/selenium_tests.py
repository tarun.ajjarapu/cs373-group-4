import unittest
from selenium import webdriver
from selenium.webdriver import ChromeOptions
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager

url = "https://www.unshakablejapan.me/"

class unshakableJapanTests(unittest.TestCase):
    def setUpTests(self):
        options = ChromeOptions()
        options.add_argument("--headless")
        options.add_argument("--window-size=1920,1080")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--no-sandbox")
        self.driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()),
                                       options=options)
        self.driver.get(url)
        self.driver.maximize_window()
        self.driver.implicitly_wait(5)

    def tearDownTests(self):
        self.driver.close()
    
    def startWebsiteTest(self):
        website_name = self.driver.find_element(By.CLASS_NAME, 'name')
        self.assertEqual(website_name, "Unshakable Japan")

    def aboutTest(self):
        self.driver.find_element(By.LINK_TEXT, "About").click()
        self.assertEqual(self.driver.current_url, url + "About")

    def prefectureTest(self):
        self.driver.find_element(By.LINK_TEXT, "Prefectures").click()
        self.assertEqual(self.driver.current_url, url + "Prefectures")

    def earthquakeTest(self):
        self.driver.find_element(By.LINK_TEXT, "Earthquakes").click()
        self.assertEqual(self.driver.current_url, url + "Earthquakes")

    def resourceTest(self):
        self.driver.find_element(By.LINK_TEXT, "Resources").click()
        self.assertEqual(self.driver.current_url, url + "Resources")

    def homeTest(self):
        self.driver.find_element(By.LINK_TEXT, "Home").click()
        self.assertEqual(self.driver.current_url, url + "Home")

    def resourceSearchTest(self):
        self.driver.get(url)
        self.driver.implicitly_wait(5)
        button = self.driver.find_element(By.XPATH, '/html/body/div/div/div/div/form/div/input')
        button.click()
        self.assertEqual(self.driver.current_url, url + "Resources")

    def prefectureSearchTest(self):
        self.driver.get(url + "Prefectures/")
        self.driver.implicitly_wait(5)
        button = self.driver.find_element(By.XPATH, '/html/body/div/div/div/div/form/div/input')
        button.click()
        self.assertEqual(self.driver.current_url, url + "Prefectures")

    def navBarTest(self):
        self.driver.get(url + 'Earthquakes/')
        self.driver.implicitly_wait(5)
        button = self.driver.find_element(By.LINK_TEXT, 'Resources')
        button.click()
        self.assertEqual(self.driver.current_url, url + "Resourecs")

    def goBackTest(self):
        self.driver.get(url + 'Prefectures/Aechi')
        self.driver.implicitly_wait(5)
        button = self.driver.find_element(By.XPATH, '/html/body/div/div/div/div[3]/div/button[0]')
        button.click()
        self.assertEqual(self.driver.current_url, url + 'Prefectures')

    def cardTest(self):
        self.driver.get(url + 'Resources')
        self.driver.implicitly_wait(5)
        button = self.driver.find_element(By.XPATH, '/html/body/div/div/div/div/div/div/div/a/button[0]')
        button.click()
        self.assertEqual(self.driver.current_url, url + "Resources/test_resource")

