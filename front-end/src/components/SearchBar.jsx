import React, { useState } from "react";
import { Form, FormControl, Button, InputGroup } from "react-bootstrap";

const SearchBar = ({ placeholder = "Eg: Noto earthquake...", onSubmit }) => {
  const [search, setSearch] = useState("");

  return (
    <Form
      onSubmit={(e) => {
        e.preventDefault();
        onSubmit(search);
      }}
      className="d-flex justify-content-center"
    >
      <InputGroup  style={{ maxWidth: "50rem" }}>
        <Form.Control
          placeholder={placeholder}
          aria-label="earthquake"
          aria-describedby="search"
          className="max-w-20 px-5 "
          onChange={(e) => setSearch(e.target.value)}
          value={search}
        />
        <InputGroup.Text
          onClick={() => onSubmit(search)}
          className="p-3 bg-accent btn"
          id="search"
        >
          🔎
        </InputGroup.Text>
      </InputGroup>
    </Form>
  );
};

export default SearchBar;
