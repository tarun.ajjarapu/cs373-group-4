import * as d3 from "d3";
import { useRef, useEffect } from "react";

export default function PieChart({
  pie_name,
  pie_value,
  total_name = "japan",
  total = 125700000,
  width = 100,
  height = 100,
}) {
  const data = [
    { name: pie_name, value: pie_value },
    { name: total_name, value: total - pie_value },
  ];
  const radius = Math.min(width, height) / 2;
  const pieGenerator = d3.pie().value((d) => d.value);
  const pie = pieGenerator(data);
  const arcPathGenerator = d3.arc();
  const arcs = pie.map((p) =>
    arcPathGenerator({
      innerRadius: 0,
      outerRadius: radius,
      startAngle: p.startAngle,
      endAngle: p.endAngle,
    })
  );
  const colors = ["#FC5656CC", "#d3d3d3"];

  return (
    <svg width={width} height={height}>
      <g transform={`translate(${width / 2}, ${height / 2})`}>
        {arcs.map((arc, i) => (
          <path key={i} d={arc} fill={colors[i]} />
        ))}
      </g>
    </svg>
  );
}

/* <PieChart pie_name="aichi" pie_percent={22.5} /> */
