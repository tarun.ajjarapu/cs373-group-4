import React, { useEffect, useRef } from "react";
import * as d3 from "d3";

const PieChartC = ({ data, width, height }) => {
  const svgRef = useRef();
  const legendRef = useRef();

  useEffect(() => {
    if (!svgRef.current) return;

    const svg = d3.select(svgRef.current);
    const legend = d3.select(legendRef.current);

    svg.selectAll("*").remove();
    legend.selectAll("*").remove();

    const radius = Math.min(width, height) / 2;
    const color = d3.scaleOrdinal(d3.schemeCategory10);

    const pie = d3.pie().value((d) => d.value);

    const arc = d3
      .arc()
      .outerRadius(radius - 10)
      .innerRadius(0);

    const arcs = svg
      .append("g")
      .attr("transform", `translate(${width / 2},${height / 2})`)
      .selectAll(".arc")
      .data(pie(data))
      .enter()
      .append("g")
      .attr("class", "arc");

    arcs
      .append("path")
      .attr("d", arc)
      .attr("fill", (d, i) => color(i));

    arcs
      .append("text")
      .attr("transform", (d) => `translate(${arc.centroid(d)})`)
      .attr("text-anchor", "middle")
      .attr("fill", "white") // Text color
      .attr("font-size", "14px") // Font size
      .text((d) => `${(d.data.value)}`);

    // Legend
    const legendItems = legend
      .selectAll(".legend-item")
      .data(data)
      .enter()
      .append("g")
      .attr("class", "legend-item")
      .attr("transform", (d, i) => `translate(0, ${i * 25 + 15})`);

    legendItems
      .append("rect")
      .attr("x", 0)
      .attr("y", 0)
      .attr("width", 15)
      .attr("height", 15)
      .attr("fill", (d, i) => color(i));

    legendItems
      .append("text")
      .attr("x", 20)
      .attr("y", 12)
      .text((d) => d.label);
  }, [data, height, width]);

  return (
    <div>
      <svg ref={svgRef} width={width} height={height}></svg>
      <svg ref={legendRef} width={300} height={height}></svg>
    </div>
  );
};

export default PieChartC;
