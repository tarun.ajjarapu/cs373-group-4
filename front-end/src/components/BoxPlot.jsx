import React, { useEffect, useRef } from "react";
import * as d3 from "d3";

const BoxWhiskerPlot = ({ data, width, height }) => {
  const svgRef = useRef();

  useEffect(() => {
    if (!data || data.length === 0) return;

    const margin = { top: 20, right: 40, bottom: 60, left: 40 };
    const innerWidth = width - margin.left - margin.right;
    const innerHeight = height - margin.top - margin.bottom;

    const svg = d3
      .select(svgRef.current)
      .attr("width", width)
      .attr("height", height);

    const xScale = d3
      .scaleLinear()
      .domain([d3.min(data), d3.max(data)])
      .range([margin.left, innerWidth]);

    const yScale = d3
      .scaleLinear()
      .domain([0, 1])
      .range([margin.top + innerHeight, margin.top]);

    const boxPlot = svg
      .append("g")
      .attr("transform", `translate(${margin.left}, ${margin.top})`);

    // Draw box
    boxPlot
      .append("rect")
      .attr("x", xScale(d3.quantile(data, 0.25)))
      .attr("y", margin.top)
      .attr(
        "width",
        xScale(d3.quantile(data, 0.75)) - xScale(d3.quantile(data, 0.25))
      )
      .attr("height", innerHeight)
      .attr("fill", "#69b3a2")
      .attr("stroke", "black");

    // Draw whiskers
    boxPlot
      .append("line")
      .attr("x1", xScale(d3.min(data)))
      .attr("y1", margin.top + innerHeight / 2)
      .attr("x2", xScale(d3.quantile(data, 0.25)))
      .attr("y2", margin.top + innerHeight / 2)
      .attr("stroke", "black");

    boxPlot
      .append("line")
      .attr("x1", xScale(d3.max(data)))
      .attr("y1", margin.top + innerHeight / 2)
      .attr("x2", xScale(d3.quantile(data, 0.75)))
      .attr("y2", margin.top + innerHeight / 2)
      .attr("stroke", "black");

    // Draw median line
    boxPlot
      .append("line")
      .attr("x1", xScale(d3.quantile(data, 0.5)))
      .attr("y1", margin.top)
      .attr("x2", xScale(d3.quantile(data, 0.5)))
      .attr("y2", margin.top + innerHeight)
      .attr("stroke", "black");

    // Add x-axis
    svg
      .append("g")
      .attr("transform", `translate(40, ${margin.top + innerHeight + 25})`)
      .call(d3.axisBottom(xScale));
  }, [data, width, height]);

  return <svg ref={svgRef} />;
};

export default BoxWhiskerPlot;
