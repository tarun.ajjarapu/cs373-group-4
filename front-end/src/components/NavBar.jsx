import React from "react";
import { Link } from "react-router-dom";
import { Navbar, Nav } from "react-bootstrap";
import Container from "react-bootstrap/Container";

const NavBar = () => {
  return (
    <Navbar expand="false" className="fixed-top" bg="light" collapseOnSelect>
      <Container>
        <Navbar.Brand href="/">Unshakable Japan</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse className="justify-content-end " id="basic-navbar-nav">
          <Nav className="text-center ">
            <Nav.Link eventKey="1" as={Link} to="/about">
              About
            </Nav.Link>
            <Nav.Link eventKey="2" as={Link} to="/earthquakes">
              Earthquakes
            </Nav.Link>
            <Nav.Link eventKey="3" as={Link} to="/prefectures">
              Prefectures
            </Nav.Link>
            <Nav.Link eventKey="4" as={Link} to="/resources">
              Resources
            </Nav.Link>
            <Nav.Link eventKey="5" as={Link} to="/visualizations">
              Our Visualizations
            </Nav.Link>
            <Nav.Link eventKey="6" as={Link} to="/provider-visualizations">
              Provider Visualizations
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};
export default NavBar;
