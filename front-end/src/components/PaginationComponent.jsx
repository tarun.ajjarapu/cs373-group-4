import { Pagination } from "react-bootstrap";

export const PaginationComponent = ({
  data,
  handlePageChange,
  currentPage,
  totalPages,
}) => {
  if (data == undefined || data.length == 0) return null;
  return (
    <div className=" d-flex justify-content-center ">
      <Pagination style={{ margin: 0, display: "flex", flexWrap: "wrap" }}>
        <Pagination.Prev
          onClick={() =>
            handlePageChange(currentPage > 1 ? currentPage - 1 : 1)
          }
          disabled={currentPage === 1}
        />
        {Array.from({ length: totalPages }).map((_, index) => (
          <Pagination.Item
            key={index}
            active={index + 1 === currentPage}
            onClick={() => handlePageChange(index + 1)}
          >
            {index + 1}
          </Pagination.Item>
        ))}
        <Pagination.Next
          onClick={() =>
            handlePageChange(
              currentPage < totalPages ? currentPage + 1 : totalPages
            )
          }
          disabled={currentPage === totalPages}
        />
      </Pagination>
    </div>
  );
};
