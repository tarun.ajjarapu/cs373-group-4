import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';

import Navbar from '../components/NavBar.jsx';
import SearchBar from '../components/SearchBar.jsx';
import Home from '../pages/Home.jsx';
import About from '../pages/About.jsx'
import Prefectures from '../pages/Prefectures.jsx';
import Earthquakes from '../pages/Earthquakes.jsx';
import Resources from '../pages/Resources.jsx';

describe('Website Tests', () => {
  test('Home renders', async () => {
    render(<BrowserRouter><Home /></BrowserRouter>);
    await waitFor(() => {
      expect(screen.getByText(/Welcome to Unshakable Japan/)).toBeInTheDocument();
    });
  });
});

describe('Page Tests', () => {
  test('Home screen renders', () => {
    render(
      <BrowserRouter>
        <Home />
      </BrowserRouter>
    );

    const welcomeMessage = screen.getByText(/Welcome to Unshakable Japan/i);
    expect(welcomeMessage).toBeInTheDocument();
  });

  test('Prefecture renders', async () => {
    render(<BrowserRouter><Prefectures /></BrowserRouter>);
    await waitFor(() => {
      expect(screen.getByText('Prefectures')).toBeInTheDocument();
    });
  });

  // Fixed key for axios

  test('About renders', async () => {
    render(<BrowserRouter><About /></BrowserRouter>);
    await waitFor(() => {
      expect(screen.getByText('Meet the Team')).toBeInTheDocument();
    });
  });

  test('About has tools', async () => {
    render(<BrowserRouter><About /></BrowserRouter>);
    await waitFor(() => {
      expect(screen.getByText('Tools Used')).toBeInTheDocument();
    });
  });


  test('Resources renders', async () => {
    render(<BrowserRouter><Resources /></BrowserRouter>);
    await waitFor(() => {
      expect(screen.getByText('Resources')).toBeInTheDocument();
    });
  });

  test('Earthquakes renders', async () => {
    render(<BrowserRouter><Earthquakes /></BrowserRouter>);
    await waitFor(() => {
      expect(screen.getByText('Earthquakes')).toBeInTheDocument();
    });
  });
});

describe("NavBar component", () => {
  test("renders Navbar with correct links", () => {
    render(
      <BrowserRouter>
        <Navbar />
      </BrowserRouter>
    );

    // Check if Navbar brand is present
    expect(screen.getByText("Unshakable Japan")).toBeInTheDocument();

    // Check if all NavLinks are present with correct text and href
    const links = [
      { text: "About", href: "/about" },
      { text: "Earthquakes", href: "/earthquakes" },
      { text: "Prefectures", href: "/prefectures" },
      { text: "Resources", href: "/resources" },
    ];

    links.forEach((link) => {
      const navLink = screen.getByText(link.text);
      expect(navLink).toBeInTheDocument();
      expect(navLink).toHaveAttribute("href", link.href);
    });
  });
});

describe("SearchBar component", () => {
  test("renders search input and button", () => {
    render(<SearchBar />);

    // Check if search input is present
    const searchInput = screen.getByPlaceholderText("Eg: Noto earthquake...");
    expect(searchInput).toBeInTheDocument();

    // Check if search button is present
    const searchButton = screen.getByText("🔎");
    expect(searchButton).toBeInTheDocument();
  });

  test("calls onSubmit prop with search value when form is submitted for earthquake", () => {
    const onSubmitMock = jest.fn();
    render(<SearchBar onSubmit={onSubmitMock} />);

    const searchInput = screen.getByPlaceholderText("Eg: Noto earthquake...");
    const searchButton = screen.getByText("🔎");

    // Simulate typing into the search input
    fireEvent.change(searchInput, { target: { value: "Noto earthquake" } });

    // Simulate clicking the search button
    fireEvent.click(searchButton);

    // Check if onSubmitMock is called with the correct search value
    expect(onSubmitMock).toHaveBeenCalledWith("Noto earthquake");
  });


});