import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import BarChart from "../components/BarChart";
import PieChartC from "../components/PieChartC";
import BoxWhiskerPlot from "../components/BoxPlot";

const ProviderVisualizations = () => {
  const [disabilities, setDisabilities] = useState([]);
  const [locations, setLocations] = useState([]);
  const [resources, setResources] = useState([]);
  useEffect(() => {
    let url = `  https://api.inclusiability.me/disability/all`;
    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        setDisabilities(data);
      });

    url = `https://api.inclusiability.me/locations/all`;
    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        setLocations(data);
      });

    url = `https://api.inclusiability.me/resources/all`;
    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        setResources(data);
      });
  }, []);

  let disabilityCategory = {};
  if ("results" in disabilities) {
    disabilities.results.forEach((i) => {
      i.forEach((j) => {
        j.categories.forEach((k) => {
          disabilityCategory[k] = (disabilityCategory[k] || 0) + 1;
        });
      });
    });
  }
  const data1 = Object.entries(disabilityCategory).map(([label, value]) => ({
    label,
    value,
  }));

  let locationRating = {};
  if ("results" in locations) {
    locations.results.forEach((i) => {
      i.forEach((j) => {
        let rating = Math.floor(j.rating);
        locationRating[rating] = (locationRating[rating] || 0) + 1;
      });
    });
  }
  const data2 = Object.entries(locationRating).map(([label, value]) => ({
    label,
    value,
  }));

  let data3 = [];
  if ("results" in resources) {
    resources.results.forEach((i) => {
      i.forEach((j) => {
        data3.push(parseFloat(j.price));
      });
    });
  }

  console.log(median(data3));

  return (
    <Container className="py-5 text-center">
      <h2 className="py-4 display-4 font-weight-bold text-dark">
        Provider Visualizations
      </h2>

      <div className="d-flex flex-column gap-5">
        <div>
          <p className="lead">Frequency of Earthquakes by Magnitude</p>
          <BarChart data={data1} width={900} height={500} />
        </div>

        <div>
          <p className="lead">Ratings of Events/Accessible Locations</p>
          <PieChartC data={data2} width={500} height={400} />
        </div>
        <div>
          <p className="lead">Price of Resources</p>
          <BoxWhiskerPlot data={data3} width={800} height={250} />
        </div>
      </div>
      <div className=" text-start">
        <h2 className=" text-center py-4 display-4 font-weight-bold text-dark">
          Developer Critique
        </h2>
        <p className="lead">What did they do well? </p>{" "}
        <p>
          {" "}
          We like their simple design and easy to access instances. They also
          used bootstrap like us so their website is responsive and clean.{" "}
        </p>{" "}
        <p className="lead">How effective was their RESTful API? </p>{" "}
        <p>
          {" "}
          Their RESTful API worked well, fetching the data and displaying it on
          the page quickly. The Postman documentation is clear and exhaustive.
        </p>{" "}
        <p className="lead">
          How well did they implement your user stories?{" "}
        </p>{" "}
        <p>
          {" "}
          While they weren’t able to meet all user stories, they provided
          reasonable alternatives and met the tasks at a later date.{" "}
        </p>{" "}
        <p className="lead">What did we learn from their website? </p>{" "}
        <p>
          We learned more about the various disabilities and the places that
          cater to them. The resources section was especially insightful as we
          didn’t know about the many technologies that were available for
          disabled individuals.{" "}
        </p>{" "}
        <p className="lead">What can they do better? </p>{" "}
        <p>
          Something they can do better is to refine their instances more as some
          don’t really make sense. Such as aging being a disability. The
          filtering, sorting is also slightly confusing. There seems to be many
          options so not sure how exactly to use them.{" "}
        </p>{" "}
        <p className="lead">What puzzles us about their website? </p>{" "}
        <p>
          {" "}
          In a disability modal instance, there are 50 related cards. I was not
          sure if that was intended or by mistake. Also wish there were
          PostmanAPI and GitLab repo links on the About page.
        </p>
      </div>
    </Container>
  );
};

export default ProviderVisualizations;

function median(arr) {
  // Step 1: Sort the array
  const sortedArr = arr.slice().sort((a, b) => a - b);

  // Step 2: Determine the length of the array
  const length = sortedArr.length;

  // Step 3: Calculate the median based on array length
  if (length % 2 === 0) {
    // If the array has an even number of elements
    const midIndex = length / 2;
    return (sortedArr[midIndex - 1] + sortedArr[midIndex]) / 2;
  } else {
    // If the array has an odd number of elements
    const midIndex = Math.floor(length / 2);
    return sortedArr[midIndex];
  }
}
