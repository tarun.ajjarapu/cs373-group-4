import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Button,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";
import SearchBar from "../components/SearchBar";
import { PaginationComponent } from "../components/PaginationComponent";
import { highlightMatch } from "./Prefectures";

export const ResourceCard = ({
  id,
  name,
  info_type,
  image,
  languages,
  link,
  percent_coverage,
  disaster_types,
  highlight,
}) => (
  <Card style={{ width: "20rem", height: "31rem" }}>
    <Card.Img src={image}></Card.Img>

    <Card.Body className="d-flex flex-column justify-content-evenly">
      <Link to={link} target="_blank">
        <Card.Title>{highlightMatch(name, highlight)}</Card.Title>
      </Link>
      <Card.Text>Type: {info_type}</Card.Text>
      <Card.Text>Languages Supported: {languages.split("#").length}</Card.Text>
      <Card.Text>
        Disaster Types Supported: {disaster_types.split("#").length}
      </Card.Text>
      <Card.Text>Percent Coverage: {percent_coverage}%</Card.Text>
    </Card.Body>
    <Card.Footer>
      <Link to={`/Resources/${id}`}>
        <Button variant="primary">More Info</Button>
      </Link>
    </Card.Footer>
  </Card>
);

const Resources = () => {
  const [data, setData] = useState([]);
  const [pageData, setPageData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sortOption, setSortOption] = useState("");
  const [infoType, setInfoType] = useState("");
  const [language, setLanguage] = useState("");
  const [disasterType, setDisasterType] = useState("");
  const [orderOption, setOrderOption] = useState("Ascending");
  const [region, setRegion] = useState("");
  const [highlight, setHighlight] = useState("");

  const itemsPerPage = 12;
  const totalPages = Math.ceil(data.length / itemsPerPage);

  const getPageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  useEffect(() => {
    const url = `https://api.unshakablejapan.me/resources${sortOption || infoType || language || disasterType || region ? "?" : ""}${sortOption ? `sort=${sortOption}` : ""}${infoType ? `${sortOption ? "&" : ""}Type=${infoType}` : ""}${language ? `${sortOption || infoType ? "&" : ""}Languages=${language}` : ""}${disasterType ? `${sortOption || infoType || language ? "&" : ""}Disaster Types=${disasterType}` : ""}${region ? `${sortOption || infoType || language || disasterType ? "&" : ""}Region=${region}` : ""}`;
    fetch(url)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((data) => {
        if (orderOption === "Descending"){
          setData(data.reverse())
        } else {
          setData(data);
        }
      });
  }, [sortOption, infoType, language, disasterType, region, orderOption]);

  useEffect(() => {
    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = data.slice(indexOfFirstItem, indexOfLastItem);
    setPageData(currentItems);
  }, [data, currentPage]);

  const getFilterSelect = (setState) => (eventKey, event) => {
    setSortOption("");
    setInfoType("");
    setDisasterType("");
    setLanguage("");
    setRegion("");
    setState(eventKey);

    setCurrentPage(1);
  };

  const getOrderSelect = (eventKey, event) => {
    setOrderOption(eventKey);
    setCurrentPage(1);
  };

  const onSubmit = (query) => {
    setHighlight(query);

    fetch(`https://api.unshakablejapan.me/resources?query=${query}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((searchData) => {
        setData(searchData);
        setCurrentPage(1);
      })
      .catch((error) => {
        console.error("fetch:", error);
      });
  };

  return (
    <Container className="py-5 text-center">
      <p className="display-4 py-4">Resources</p>
      <Row className="d-flex gap-2">
        <SearchBar onSubmit={onSubmit} />

        <div className="d-flex flex-wrap justify-content-center gap-2">
          <DropdownButton
            title={`Sort by: ${sortOption || "None"}`}
            onSelect={getFilterSelect(setSortOption)}
            className="mb-3"
          >
            <Dropdown.Item eventKey="">None</Dropdown.Item>
            <Dropdown.Item eventKey="name">Name</Dropdown.Item>
            <Dropdown.Item eventKey="Percent Coverage">
              Percent Coverage
            </Dropdown.Item>
          </DropdownButton>

          <DropdownButton
            title={`Info Type: ${infoType || "All"}`}
            onSelect={getFilterSelect(setInfoType)}
            className="mb-3"
          >
            <Dropdown.Item eventKey="">All</Dropdown.Item>
            <Dropdown.Item eventKey="Damage Status">
              Damage Status
            </Dropdown.Item>
            <Dropdown.Item eventKey="Weather Status">
              Weather Status
            </Dropdown.Item>
            <Dropdown.Item eventKey="Evacuation Info">
              Evacuation Info
            </Dropdown.Item>
            <Dropdown.Item eventKey="Traffic and Logistics Info">
              Traffic and Logistics Info
            </Dropdown.Item>
            <Dropdown.Item eventKey="Disaster Victim Assistance Info">
              Disaster Victim Assistance Info
            </Dropdown.Item>
            <Dropdown.Item eventKey="Safety Status">
              Safety Status
            </Dropdown.Item>
            <Dropdown.Item eventKey="Lifeline Information">
              Lifeline Information
            </Dropdown.Item>
            <Dropdown.Item eventKey="Region Information">
              Region Information
            </Dropdown.Item>
          </DropdownButton>

          <DropdownButton
            title={`Languages: ${language || "All"}`}
            onSelect={getFilterSelect(setLanguage)}
            className="mb-3"
          >
            <Dropdown.Item eventKey="">All</Dropdown.Item>
            <Dropdown.Item eventKey="Japanese">Japanese</Dropdown.Item>
            <Dropdown.Item eventKey="English">English</Dropdown.Item>
            <Dropdown.Item eventKey="Korean">Korean</Dropdown.Item>
            <Dropdown.Item eventKey="Chinese (Traditional)">
              Chinese (Traditional)
            </Dropdown.Item>
            <Dropdown.Item eventKey="Chinese (Simplified)">
              Chinese (Simplified)
            </Dropdown.Item>
            <Dropdown.Item eventKey="Thai">Thai</Dropdown.Item>
            <Dropdown.Item eventKey="Vietnamese">Vietnamese</Dropdown.Item>
            <Dropdown.Item eventKey="Portuguese">Portuguese</Dropdown.Item>
          </DropdownButton>

          <DropdownButton
            title={`Disaster Types: ${disasterType || "All"}`}
            onSelect={getFilterSelect(setDisasterType)}
            className="mb-3"
          >
            <Dropdown.Item eventKey="">All</Dropdown.Item>
            <Dropdown.Item eventKey="Earthquake and Tsunami Disaster">
              Earthquake and Tsunami Disaster
            </Dropdown.Item>
            <Dropdown.Item eventKey="Wind and Flood Disaster">
              Wind and Flood Disaster
            </Dropdown.Item>
            <Dropdown.Item eventKey="Volcanic Disaster">
              Volcanic Disaster
            </Dropdown.Item>
            <Dropdown.Item eventKey="Snow Disaster">
              Snow Disaster
            </Dropdown.Item>
            <Dropdown.Item eventKey="Other">Other</Dropdown.Item>
          </DropdownButton>

          <DropdownButton
            title={`Region: ${region || "All"}`}
            onSelect={getFilterSelect(setRegion)}
            className="mb-3"
          >
            <Dropdown.Item eventKey="">All</Dropdown.Item>
            <Dropdown.Item eventKey="Chubu">Chubu</Dropdown.Item>
            <Dropdown.Item eventKey="Hokkaido">Hokkaido</Dropdown.Item>
            <Dropdown.Item eventKey="Tohoku">Tohoku</Dropdown.Item>
            <Dropdown.Item eventKey="Kanto">Kanto</Dropdown.Item>
            <Dropdown.Item eventKey="Shikoku">Shikoku</Dropdown.Item>
            <Dropdown.Item eventKey="Hokuriku">Hokuriku</Dropdown.Item>
            <Dropdown.Item eventKey="Kinki">Kinki</Dropdown.Item>
            <Dropdown.Item eventKey="Chugoku">Chugoku</Dropdown.Item>
            <Dropdown.Item eventKey="Kyushu">Kyushu</Dropdown.Item>
          </DropdownButton>
          <DropdownButton
            title={`Order by: ${orderOption || "Ascending"}`}
            onSelect={getOrderSelect}
            className="mb-3"
          >
            <Dropdown.Item eventKey="Ascending">Ascending</Dropdown.Item>
            <Dropdown.Item eventKey="Descending">Decending</Dropdown.Item>
          </DropdownButton>
        </div>

        <div className="d-flex flex-wrap justify-content-center align-items-center gap-4 py-4">
          {pageData.map((props, i) => (
            <ResourceCard key={i} highlight={highlight} {...props} />
          ))}
        </div>

        <PaginationComponent
          data={data}
          getPageChange={getPageChange}
          currentPage={currentPage}
          totalPages={totalPages}
        />

        <p>Number of Resources: {data.length}</p>
      </Row>
    </Container>
  );
};

export default Resources;
