import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Button,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";
import SearchBar from "../components/SearchBar";
import { PaginationComponent } from "../components/PaginationComponent";

export const highlightMatch = (name, query) => {
  if (!query) {
    return name;
  }
  const regex = new RegExp(`(${query})`, "gi");
  const parts = name.split(regex);
  return parts.map((part, index) => {
    if (regex.test(part)) {
      return (
        <span key={index} style={{ backgroundColor: "yellow" }}>
          {part}
        </span>
      );
    } else {
      return part;
    }
  });
};

export const PrefectureCard = ({
  name,
  island,
  population,
  risk,
  density,
  id,
  highlight,
}) => (
  <Card style={{ width: "20rem", height: "20rem" }}>
    <Card.Body className="d-flex flex-column justify-content-evenly">
      <Card.Title>{highlightMatch(name, highlight)}</Card.Title>
      <div className="">
        <Card.Text>Island: {island}</Card.Text>
        <Card.Text>Population: {population.toLocaleString()}</Card.Text>
        <Card.Text>Density: {density}</Card.Text>
        <Card.Text>Risk Score: {risk}%</Card.Text>
      </div>
    </Card.Body>
    <Card.Footer>
      <Link to={`/prefectures/${id}`}>
        <Button variant="primary">More Info</Button>
      </Link>
    </Card.Footer>
  </Card>
);

const Prefectures = () => {
  const [data, setData] = useState([]);
  const [pageData, setPageData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedIsland, setSelectedIsland] = useState("");
  const [sortOption, setSortOption] = useState("");
  const [orderOption, setOrderOption] = useState("Ascending");
  const [highlight, setHighlight] = useState("");
  const itemsPerPage = 12;
  const totalPages = Math.ceil(data.length / itemsPerPage);
  const getPageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };
  useEffect(() => {
    const url = `https://api.unshakablejapan.me/prefectures${selectedIsland ? `?Island=${selectedIsland.toLowerCase()}` : ""}${sortOption ? `${selectedIsland ? "&" : "?"}sort=${sortOption}` : ""}`;
    fetch(url)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((data) => {
        if (orderOption === "Descending"){
          setData(data.reverse())
        } else {
          setData(data);
        }
      });
  }, [selectedIsland, sortOption, orderOption]);
  const onSubmit = (query) => {
    setHighlight(query);
    fetch(`https://api.unshakablejapan.me/prefectures?query=${query}`)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((searchData) => {
        setData(searchData);
        setCurrentPage(1);
      });
  };
  useEffect(() => {
    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = data.slice(indexOfFirstItem, indexOfLastItem);
    setPageData(currentItems);
  }, [data, currentPage]);

  const getIslandSelect = (eventKey, event) => {
    setSelectedIsland(eventKey);
    setCurrentPage(1);
  };

  const getSortSelect = (eventKey, event) => {
    setSortOption(eventKey);
    setCurrentPage(1);
  };

  const getOrderSelect = (eventKey, event) => {
    setOrderOption(eventKey);
    setCurrentPage(1);
  };

  return (
    <Container className="py-5 text-center">
      <p className="display-4 py-4 ">Prefectures</p>
      <Row className="d-flex gap-2">
        <SearchBar onSubmit={onSubmit} placeholder="Eg: Aichi..." />
        <div className="d-flex justify-content-center gap-2">
          <DropdownButton
            title={`Island: ${selectedIsland || "None"}`}
            onSelect={getIslandSelect}
            className="mb-3"
          >
            <Dropdown.Item eventKey="">None</Dropdown.Item>
            <Dropdown.Item eventKey="Hokkaido">Hokkaido</Dropdown.Item>
            <Dropdown.Item eventKey="Honshu">Honshu</Dropdown.Item>
            <Dropdown.Item eventKey="Kyushu">Kyushu</Dropdown.Item>
            <Dropdown.Item eventKey="Ryukyu Islands">
              Ryukyu Islands
            </Dropdown.Item>
            <Dropdown.Item eventKey="Shikoku">Shikoku</Dropdown.Item>
          </DropdownButton>
          <DropdownButton
            title={`Sort by: ${sortOption || "None"}`}
            onSelect={getSortSelect}
            className="mb-3"
          >
            <Dropdown.Item eventKey="">None</Dropdown.Item>
            <Dropdown.Item eventKey="name">Name</Dropdown.Item>
            <Dropdown.Item eventKey="island">Island</Dropdown.Item>
            <Dropdown.Item eventKey="density">Density</Dropdown.Item>
            <Dropdown.Item eventKey="population">Population</Dropdown.Item>
            <Dropdown.Item eventKey="risk">Risk Score</Dropdown.Item>
          </DropdownButton>
          <DropdownButton
            title={`Order by: ${orderOption || "Ascending"}`}
            onSelect={getOrderSelect}
            className="mb-3"
          >
            <Dropdown.Item eventKey="Ascending">Ascending</Dropdown.Item>
            <Dropdown.Item eventKey="Descending">Decending</Dropdown.Item>
          </DropdownButton>
        </div>
        <div className="d-flex flex-wrap justify-content-center align-items-center gap-4 py-4">
          {pageData.map((props, i) => (
            <PrefectureCard key={i} highlight={highlight} {...props} />
          ))}
        </div>
        <PaginationComponent
          data={data}
          getPageChange={getPageChange}
          currentPage={currentPage}
          totalPages={totalPages}
        />
        <p>Number of Prefectures: {data.length}</p>
      </Row>
    </Container>
  );
};

export default Prefectures;
