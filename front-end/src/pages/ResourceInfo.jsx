import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import { Image } from "react-bootstrap";
import PieChart from "../components/PieChart";
import { select } from "./EarthquakeInfo";
import { PrefectureCard } from "./Prefectures";
import { EarthquakeCard } from "./Earthquakes";

const ResourceInfo = () => {
  const { resourceId } = useParams();
  const [data, setData] = useState({});
  const [prefectureData, setPrefectureData] = useState([]);
  const [earthquakeData, setEarthquakeData] = useState([]);

  useEffect(() => {
    fetch(`https://api.unshakablejapan.me/resources/${resourceId}`)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((data) => {
        setData(data);
      });
  }, []);

  useEffect(() => {
    if ("prefectures" in data) {
      const tempPrefectures = select(data.prefectures, resourceId);
      const fetchData = async () => {
        const fetchPromises = tempPrefectures.map((prefecture) =>
          fetch(
            `https://api.unshakablejapan.me/prefectures/${prefecture.id}`
          ).then((response) => {
            if (response.ok) {
              return response.json();
            }
          })
        );

        try {
          const prefecturesData = await Promise.all(fetchPromises);
          setPrefectureData(prefecturesData);
        } catch (error) {
          console.error("Error", error);
        }
      };

      fetchData();
    }
  }, [data]);

  useEffect(() => {
    if (prefectureData.length !== 0 && "earthquakes" in prefectureData[0]) {
      const fetchData = async () => {
        const fetchPromises = prefectureData.map((prefecture) => {
          const earthquake = prefecture.earthquakes[0];
          return fetch(
            `https://api.unshakablejapan.me/earthquakes/${earthquake.id}`
          ).then((response) => {
            if (response.ok) {
              return response.json();
            }
          });
        });

        try {
          const earthquakesData = await Promise.all(fetchPromises);
          setEarthquakeData(earthquakesData);
        } catch (error) {
          console.error("Error:", error);
        }
      };

      fetchData();
    }
  }, [prefectureData]);

  return (
    <Container className="py-5 ">
      <h2 className="py-4 display-2 font-weight-bold text-dark">{data.name}</h2>
      <div className="d-flex flex-wrap justify-content-between">
        <div>
          <p className="">Languages:</p>

          <ul className=" lead">
            {data.languages &&
              data.languages.split("#").map((i) => <li>{i}</li>)}
          </ul>
          <p className="">Disaster Types:</p>
          <ul className=" lead">
            {data.disaster_types &&
              data.disaster_types.split("#").map((i) => <li>{i}</li>)}
          </ul>
          <p className="display-6">Resource Type: {data.info_type}</p>
          <Link to={data.link} target="_blank" className="lead">
            Link to Resource
          </Link>
        </div>
        <div className="d-flex flex-column gap-4 justify-content-center align-items-center">
          <Image
            className="p-2"
            style={{ maxWidth: "500px" }}
            src={data.image}
          />
          <div className="d-flex flex-column gap-2 justify-content-between align-items-center">
            <PieChart
              pie_name={"pi"}
              pie_value={data.percent_coverage}
              total={100}
            />
            <p className="d-flex align-items-center gap-2">
              Percent of Regions Covered:
              <span className=" display-6 ">{data.percent_coverage} %</span>
            </p>
          </div>
        </div>
      </div>
      <div className="pt-4 d-flex flex-wrap gap-4  justify-content-center align-items-start text-center">
        <div className="text-center ">
          <p>Related Earthquake</p>
          <div className="d-flex flex-wrap justify-content-center gap-2">
            {earthquakeData.length !== 0 &&
              earthquakeData.map((earthquakeProps) => (
                <EarthquakeCard {...earthquakeProps} />
              ))}
          </div>
        </div>
        <div className="text-center  ">
          <p>Related Prefecture</p>
          {Object.keys(prefectureData).length !== 0 && (
            <div className="d-flex flex-wrap justify-content-center gap-2">
              {prefectureData.map((prefectureProps, index) => (
                <PrefectureCard key={index} {...prefectureProps} />
              ))}
            </div>
          )}
        </div>
      </div>
    </Container>
  );
};

export default ResourceInfo;
