import React, { useState, useEffect } from "react";
import {
  Container,
  Row,
  Button,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import SearchBar from "../components/SearchBar";
import { PaginationComponent } from "../components/PaginationComponent";
import { highlightMatch } from "./Prefectures";

export const EarthquakeCard = ({
  name,
  date,
  magnitude,
  casualties,
  year,
  id,
  highlight,
}) => (
  <Card style={{ width: "20rem", height: "20rem" }}>
    <Card.Body className="d-flex flex-column justify-content-evenly">
      <Card.Title>{highlightMatch(name, highlight)}</Card.Title>
      <div className="">
        <Card.Text>Date: {date}</Card.Text>
        <Card.Text>Year: {year}</Card.Text>
        <Card.Text>Magnitude: {magnitude}</Card.Text>
        <Card.Text>Casualties: {casualties}</Card.Text>
      </div>
    </Card.Body>
    <Card.Footer>
      <Link to={`/earthquakes/${id}`}>
        <Button variant="primary">More Info</Button>
      </Link>
    </Card.Footer>
  </Card>
);

const Earthquakes = () => {
  const [data, setData] = useState([]);
  const [pageData, setPageData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sortOption, setSortOption] = useState("");
  const [orderOption, setOrderOption] = useState("Ascending");
  const [selectedMonth, setSelectedMonth] = useState("");
  const [highlight, setHighlight] = useState("");

  const itemsPerPage = 12;
  const totalPages = Math.ceil(data.length / itemsPerPage);

  const getPageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const onSubmit = (query) => {
    setHighlight(query);

    fetch(`https://api.unshakablejapan.me/earthquakes?query=${query}`)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((searchData) => {
        setData(searchData);
        setCurrentPage(1);
      });
  };
  useEffect(() => {
    const url = `https://api.unshakablejapan.me/earthquakes${sortOption || selectedMonth ? "?" : ""}${sortOption ? `sort=${sortOption}` : ""}${selectedMonth ? `${sortOption ? "&" : ""}Date=${selectedMonth}` : ""}`;
    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Error");
        }
        return response.json();
      })
      .then((data) => {
        if (orderOption === "Descending"){
          setData(data.reverse())
        } else {
          setData(data);
        }
      })
      .catch((error) => {
        console.error("fetch:", error);
      });
  }, [sortOption, selectedMonth, orderOption]);
  useEffect(() => {
    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = data.slice(indexOfFirstItem, indexOfLastItem);
    setPageData(currentItems);
  }, [data, currentPage]);

  const getSortSelect = (eventKey, event) => {
    setSortOption(eventKey);
    setCurrentPage(1);
  };
  const getMonthSelect = (eventKey, event) => {
    setSelectedMonth(eventKey);
    setCurrentPage(1);
  };
  const getOrderSelect = (eventKey, event) => {
    setOrderOption(eventKey);
    setCurrentPage(1);
  };
  return (
    <Container className="py-5 text-center">
      <p className="display-4 py-4">Earthquakes</p>
      <Row className="d-flex gap-2">
        <SearchBar placeholder="Eg: Noto earthquake..." onSubmit={onSubmit} />
        <div className="d-flex justify-content-center gap-2">
          <DropdownButton
            title={`Sort by: ${sortOption || "None"}`}
            onSelect={getSortSelect}
            className="mb-3"
          >
            <Dropdown.Item eventKey="">None</Dropdown.Item>
            <Dropdown.Item eventKey="name">Name</Dropdown.Item>
            <Dropdown.Item eventKey="year">Year</Dropdown.Item>
            <Dropdown.Item eventKey="magnitude">Magnitude</Dropdown.Item>
            <Dropdown.Item eventKey="casualties">Casualties</Dropdown.Item>
          </DropdownButton>

          <DropdownButton
            title={`Month: ${selectedMonth || "All"}`}
            onSelect={getMonthSelect}
            className="mb-3"
          >
            <Dropdown.Item eventKey="">All</Dropdown.Item>
            <Dropdown.Item eventKey="January">January</Dropdown.Item>
            <Dropdown.Item eventKey="February">February</Dropdown.Item>
            <Dropdown.Item eventKey="March">March</Dropdown.Item>
            <Dropdown.Item eventKey="April">April</Dropdown.Item>
            <Dropdown.Item eventKey="May">May</Dropdown.Item>
            <Dropdown.Item eventKey="June">June</Dropdown.Item>
            <Dropdown.Item eventKey="July">July</Dropdown.Item>
            <Dropdown.Item eventKey="August">August</Dropdown.Item>
            <Dropdown.Item eventKey="September">September</Dropdown.Item>
            <Dropdown.Item eventKey="October">October</Dropdown.Item>
            <Dropdown.Item eventKey="November">November</Dropdown.Item>
            <Dropdown.Item eventKey="December">December</Dropdown.Item>
          </DropdownButton>
          <DropdownButton
            title={`Order by: ${orderOption || "Ascending"}`}
            onSelect={getOrderSelect}
            className="mb-3"
          >
            <Dropdown.Item eventKey="Ascending">Ascending</Dropdown.Item>
            <Dropdown.Item eventKey="Descending">Decending</Dropdown.Item>
          </DropdownButton>
        </div>
        <div className="d-flex flex-wrap justify-content-center align-items-center gap-4">
          {pageData.map((props, i) => (
            <EarthquakeCard key={i} highlight={highlight} {...props} />
          ))}
        </div>

        <PaginationComponent
          data={data}
          getPageChange={getPageChange}
          currentPage={currentPage}
          totalPages={totalPages}
        />

        <p>Number of Earthquakes: {data.length}</p>
      </Row>
    </Container>
  );
};

export default Earthquakes;
