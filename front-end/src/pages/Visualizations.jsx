import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import BarChart from "../components/BarChart";
import PieChartC from "../components/PieChartC";
import ScatterPlot from "../components/ScatterPlot";

const Visualizations = () => {
  const [earthquake1, setEarthquakeData1] = useState([]);
  const [resourcesData, setResourcesData] = useState([]);
  const [earthquake2, setEarthquake2] = useState([]);

  console.log(earthquake2);

  useEffect(() => {
    let url = `https://api.unshakablejapan.me/earthquakes`;
    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw new Error("bad response");
        }
        return response.json();
      })
      .then((data) => {
        setEarthquakeData1(data);
        const filteredData = data.filter(
          (d) => d.year >= 900 && d.casualties !== 0
        );
        setEarthquake2(
          filteredData.map((d) => ({
            year: d.year,
            casualties: d.casualties,
          }))
        );
      })
      .catch((error) => {
        console.error("fetch", error);
      });
    url = `https://api.unshakablejapan.me/resources`;
    fetch(url)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((data) => {
        setResourcesData(data);
      });
  }, []);

  let resourcesType = {};
  resourcesData.forEach((i) => {
    resourcesType[i.info_type] = (resourcesType[i.info_type] || 0) + 1;
  });
  const data2 = Object.entries(resourcesType).map(([label, value]) => ({
    label,
    value,
  }));

  let earthquakeMagnitude = {};
  earthquake1.forEach((i) => {
    const magnitude = Math.floor(i.magnitude);
    earthquakeMagnitude[magnitude] = (earthquakeMagnitude[magnitude] || 0) + 1;
  });
  const data1 = Object.entries(earthquakeMagnitude).map(([label, value]) => ({
    label,
    value,
  }));

  return (
    <Container className="py-5 text-center">
      <h2 className="py-4 display-4 font-weight-bold text-dark">
        Our Visualizations
      </h2>
      <div className="d-flex flex-column gap-5">
        <div>
          <p className="lead">Frequency of Earthquakes by Magnitude</p>
          <BarChart data={data1} width={500} height={500} />
        </div>

        <div>
          <p className="lead">Resources Type Distribution</p>
          <PieChartC data={data2} width={500} height={400} />
        </div>
        <div>
          <p className="lead">Casualties vs Time (Year)</p>
          <ScatterPlot data={earthquake2} width={500} height={500} />
        </div>
        <div className=" text-start">
          <h2 className=" text-center py-4 display-4 font-weight-bold text-dark">
            Self Critique
          </h2>
          <p className="lead">What did we do well?</p>{" "}
          <p>
            Our UI is well done and responsive. We made sure to spend time to
            make our website look professional. We communicated effectively
            between frontend and backend to plan, design, and execute features.
          </p>{" "}
          <p className="lead">What did we learn?</p>{" "}
          <p>
            We learned how to use Docker, Selenium, and other tools we hadn’t
            used before. We also learned how to work effectively in a medium
            sized group on a project that had multiple checkpoints. Hosting the
            backend and frontend was something new we learned.
          </p>{" "}
          <p className="lead">What did we teach each other?</p>{" "}
          <p>
            We taught each other on the topics we had prior experience to. So
            everyone learned a little about front end styling, back end API
            calls, and testing.
          </p>{" "}
          <p className="lead">What can we do better?</p>{" "}
          <p>
            Something we can do better is managing a work schedule. A lot of the
            time we would be working on the project on our own async and
            wouldn’t really meet up to go over progress checks. Still, we were
            able to make progress and finish the objectives for each phase
          </p>{" "}
          <p className="lead">What effect did the peer reviews have?</p>{" "}
          <p>
            The peer reviews were helpful to see how others were viewing your
            work and to see what you could work on for the next phase. But,
            usually the feedback we got were things we were already aware of.
          </p>{" "}
          <p className="lead">What puzzles us?</p>{" "}
          <p>
            The EC2 backend hosting and Elastic Load Balancer was confusing. The
            TA tutorial slides, while helpful, were just very long and did not
            help me understand what I was doing.
          </p>
        </div>
      </div>
    </Container>
  );
};

export default Visualizations;
