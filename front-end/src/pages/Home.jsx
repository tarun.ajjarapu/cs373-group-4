import React, { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import { Link, useNavigate } from "react-router-dom";
import SearchBar from "../components/SearchBar";
const emergencyContactData = [
  {
    title: "📞 Dial 171",
    desc: "The 171 Emergency Line is a voice message board service provided by NTT that is available when a disaster occurs.",
    link: "https://www.japanlivingguide.com/health-and-safety/emergency/emergency-contact-methods/",
  },
  {
    title: "What to do During an Earthquake",
    desc: "Steps on what to do during an earthquake to keep yourself safe.",
    link: "https://arigatojapan.co.jp/earthquake-safety-japan/#:~:text=If%20inside%20a%20building%3A%20Drop,onto%20the%20furniture%20for%20stability.",
  },
];

const InfoCard = ({ title, desc, link }) => {
  return (
    <Card style={{ width: "100%" }}>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Text>{desc}</Card.Text>
        <Card.Link href={link} target="_blank">
          Learn More
        </Card.Link>
      </Card.Body>
    </Card>
  );
};

function Home() {
  const [index, setIndex] = useState(1);
  const numImages = 4;
  const navigate = useNavigate();

  useEffect(() => {
    const intervalId = setInterval(() => {
      setIndex((prevIndex) => (prevIndex % numImages) + 1);
    }, 5000);
    return () => clearInterval(intervalId);
  }, []);
  const linkStyle = {
    textDecoration: "none !important",
  };
  const onSubmit = (searchQuery) => {
    navigate(`/allSearch/${(searchQuery)}`);
  };
  return (
    <div className="py-5 ">
      <img
        src={require(`../assets/splash/jp_${index}.jpg`)}
        alt={`jp_${index}`}
        className="splash-img position-absolute top-0 start-0 w-100"
        style={{ height: "800px" }}
      />
      <div id="splash-img" className="splash-img">
        <Container className="d-flex justify-content-center  ">
          <Col xs={10} md={8} className=" text-md-left">
            <div className="splash-card p-5 " style={{ borderRadius: "1rem" }}>
              <h1 className=" display-2 font-weight-bold text-dark">
                Welcome to Unshakable Japan
              </h1>
              <div className="pt-2 ">
                <p className="lead fs-4 ">
                  Find vital insights into earthquake occurrences and their
                  impact on Japanese prefectures.
                </p>
                <SearchBar
                  onSubmit={onSubmit}
                  placeholder="Eg: Tensho Earthquake, Aichi, Airport and Airline Status,..."
                />
                <ListGroup horizontal className="pt-2 d-flex flex-wrap w-100">
                  <Link
                    to="/prefectures"
                    className="flex-grow-1"
                    style={{
                      textDecoration: "none",

                      borderRadius: ".5rem 0rem 0rem .5rem",
                    }}
                  >
                    <ListGroup.Item className="bg-accent ">
                      🇯🇵 Prefectures
                    </ListGroup.Item>
                  </Link>
                  <Link
                    className="flex-grow-1"
                    to="/earthquakes"
                    style={{
                      textDecoration: "none",
                    }}
                  >
                    <ListGroup.Item className="bg-accent ">
                      ⛔ Earthquakes
                    </ListGroup.Item>
                  </Link>
                  <Link
                    to="/resources"
                    className="flex-grow-1"
                    style={{
                      textDecoration: "none",

                      borderRadius: "0rem .5rem .5rem 0rem",
                    }}
                  >
                    <ListGroup.Item className="bg-accent ">
                      ✊ Resources
                    </ListGroup.Item>
                  </Link>
                </ListGroup>
              </div>
            </div>
          </Col>
        </Container>
      </div>
      <Container className="my-5 gap-5 d-flex flex-column justify-content-center align-items-start">
        <Row className="d-flex gy-5">
          <Col xs={12} md={6}>
            <iframe
              className="iframe"
              src="https://www.youtube.com/embed/e3Dn7MJaMuI?si=9lu9Hh0w_eW97vd2"
            ></iframe>
          </Col>
          <Col className="d-flex flex-wrap gap-2 justify-content-start">
            <h1 className=" display-6 mb-2">Emergency Info</h1>
            {emergencyContactData.map((info, index) => (
              <InfoCard
                key={index}
                title={info.title}
                desc={info.desc}
                link={info.link}
              />
            ))}
          </Col>
        </Row>
        {}
      </Container>
    </div>
  );
}

export default Home;
