import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { useParams } from "react-router-dom";
import createMapsURL from "../services/googleMaps";
import { PrefectureCard } from "./Prefectures";
import { ResourceCard } from "./Resources";
import PieChart from "../components/PieChart";

const fallback = [
  "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/EQs_1900-2016_Japan_tsum.png/300px-EQs_1900-2016_Japan_tsum.png",
  "https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Chuurei-tou_Fujiyoshida_17025277650_c59733d6ba_o.jpg/220px-Chuurei-tou_Fujiyoshida_17025277650_c59733d6ba_o.jpg",
  "https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Japan_Trench.png/240px-Japan_Trench.png",
  "https://upload.wikimedia.org/wikipedia/commons/d/d3/Fukushima_Earthquake_Shakemap_20161122.png",
  "https://qph.cf2.quoracdn.net/main-qimg-ea0c1f51dca5ee9ce019dba04c231932-pjlq",
];

const EarthInfo = () => {
  const { earthquakeId } = useParams();
  const [data, setData] = useState({});
  const [prefectureData, setPrefectureData] = useState({});
  const [resourcesData, setResourcesData] = useState([]);

  async function getWikiImage(name, fallback) {
    fetch(
      `https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles=${name}&origin=*`
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((data) => {
        if (data.query.pages[Object.keys(data.query.pages)[0]].original) {
          setData((prevData) => {
            return {
              ...prevData,
              image:
                data.query.pages[Object.keys(data.query.pages)[0]].original
                  .source,
            };
          });
        } else {
          setData((prevData) => {
            return {
              ...prevData,
              image: getRand(fallback, 1),
            };
          });
        }
      });
  }
  useEffect(() => {
    fetch(`https://api.unshakablejapan.me/earthquakes/${earthquakeId}`)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then(async (data) => {
        setData(data);
        await getWikiImage(data.name, fallback);
      });
  }, []);
  useEffect(() => {
    if ("prefecture_id" in data) {
      fetch(`https://api.unshakablejapan.me/prefectures/${data.prefecture_id}`)
        .then((response) => {
          if (response.ok) {
            return response.json();
          }
        })
        .then((data) => {
          setPrefectureData(data);
        });
    }
  }, [data]);
  useEffect(() => {
    if (Object.keys(prefectureData).length !== 0) {
      const resourceIds = select(prefectureData.resources, earthquakeId);
      const fetchData = async () => {
        const fetchPromises = resourceIds.map((resourceId) =>
          fetch(
            `https://api.unshakablejapan.me/resources/${resourceId.id}`
          ).then((response) => {
            if (response.ok) {
              return response.json();
            }
          })
        );

        try {
          const resourcesData = await Promise.all(fetchPromises);
          setResourcesData(resourcesData);
        } catch (error) {
          console.error("Error", error);
        }
      };

      fetchData();
    }
  }, [prefectureData]);

  return (
    <Container className="py-5 ">
      <h2 className="py-4 display-2 font-weight-bold text-dark">{data.name}</h2>
      <div className="d-flex flex-wrap justify-content-between">
        <div>
          <p className=" lead">Date: {data.date}</p>
          <p className=" lead">Year: {data.year}</p>
          <p className=" lead">
            Casualties:{" "}
            {data.casualties == 0
              ? "0"
              : parseInt(data.casualties).toLocaleString()}
          </p>
          <p className="lead">
            Coordinates: ({data.latitude}, {data.longitude})
          </p>
          <div className="d-flex gap-4">
            <p className="display-6 ">Magnitude: {data.magnitude}</p>
            <PieChart pie_value={data.magnitude} total={9.5} />
          </div>
        </div>
        <img className="img-fluid" src={data.image} alt="" width={500} />
      </div>
      <div className="py-5 d-flex flex-wrap gap-2 justify-content-center align-items-start">
        {data.longitude && data.latitude && (
          <iframe
            title="Embedded Map"
            src={createMapsURL(data.latitude, data.longitude, 8, "satellite")}
            width="100%"
            height="400px"
            border="0"
            allowFullScreen=""
            aria-hidden="false"
            tabIndex="0"
          />
        )}
      </div>

      <div className="d-flex flex-wrap gap-4  justify-content-center align-items-start text-center">
        <div className="text-center ">
          <p>Related Prefecture</p>
          {Object.keys(prefectureData).length !== 0 && (
            <PrefectureCard {...prefectureData} />
          )}
        </div>
        <div className="text-center d-flex flex-column ">
          <p>Related Resources</p>
          {Object.keys(resourcesData).length !== 0 && (
            <div className="d-flex flex-wrap justify-content-center gap-2">
              {resourcesData.map((resourceProps) => (
                <ResourceCard {...resourceProps} />
              ))}
            </div>
          )}
        </div>
      </div>
    </Container>
  );
};

export default EarthInfo;

export function select(array, earthquakeId) {
  const firstDigit = parseInt(earthquakeId.toString()[0]);
  const start = Math.min(firstDigit, array.length - 1);
  const end = Math.min(start + 3, array.length);
  return array.slice(start, end);
}

export function getRand(array, n) {
  if (n >= array.length) {
    return array.slice();
  }
  let shuffledArray = array.slice();
  for (let i = shuffledArray.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
  }
  return shuffledArray.slice(0, n);
}
