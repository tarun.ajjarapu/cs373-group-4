import React, { useEffect, useState, useRef } from "react";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import axios from "axios";
import Container from "react-bootstrap/Container";
import Ratio from "react-bootstrap/Ratio";
import Image from "react-bootstrap/Image";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";

const aboutCardData = [
  {
    img: "shiva.jpg",
    name: ["Shiva Balathandayuthapani", "Shiva", "shaank", "shivabalanb"],
    bio: "I'm a second-year CS major at UT Austin with interests in full stack and blockchain technologies. I enjoy playing basketball and volunteering.",
    role: "Frontend",
    unit_tests: 2,
  },
  {
    img: "tarun.jpg",
    name: ["Tarun Ajjarapu"],
    bio: "I'm a second-year CS major at UT Austin with a minor in business. I play competitive tennis and enjoy playing basketball.",
    role: "Backend",
    unit_tests: 4,
  },
  {
    img: "ben.jpg",
    name: ["Benjamin Truong", "Ben Truong"],
    bio: "Hi! I'm a third year CS major at UT Austin. I enjoy guitar, ultimate frisbee, and jazz.",
    role: "Backend",
    unit_tests: 2,
  },
  {
    img: "mohammad.png",
    name: ["Mohammad Hakim", "mohammad-hakim", "Mohammad S Hakim"],
    bio: "I'm a third year CS major at UT Austin with interests in software engineering and machine learning.",
    role: "Frontend",
    unit_tests: 4,
  },
  {
    img: "jose.jpg",
    name: ["Jose Quezada", "jose_r_quezada"],
    bio: "I'm a third-year CS major at UT Austin with interests in software development management. I do archery and enjoy listening to music.",
    role: "Frontend",
    unit_tests: 2,
  },
];

const dataSources = [
  {
    name: "World data",
    link: "https://www.worlddata.info/asia/japan/earthquakes.php",
  },
  {
    name: "Wikipedia(RESTful API)",
    link: "https://en.wikipedia.org/wiki/List_of_earthquakes_in_Japan",
  },
  {
    name: "Disaster Prevention Portal",
    link: "https://www.mlit.go.jp/river/bousai/olympic/en/helpful03/index.html",
  },
];

const toolCardData = [
  { name: "Bootstrap", imgName: "bootstrap.png" },
  { name: "ReactJS", imgName: "reactjs.png" },
  { name: "Docker", imgName: "docker.png" },
  { name: "VSCode", imgName: "vscode.png" },
  { name: "Postman", imgName: "postman.png" },
  { name: "AWS Amplify", imgName: "amplify.png" },
];

function AboutCard({ img, name, bio, role, commits, issues, unit_tests }) {
  return (
    <Card style={{ width: "20rem" }}>
      <Ratio aspectRatio="4x3">
        <Image
          loading="lazy"
          className="p-2 "
          src={require(`../assets/about/${img}`)}
        />
      </Ratio>

      <Card.Body>
        <Card.Title>{name[0]}</Card.Title>
        <Card.Text>{bio}</Card.Text>
        <Card.Text>Role: {role}</Card.Text>
        <ListGroup>
          <ListGroup.Item>
            Commits: <span style={{ float: "right" }}>{commits}</span>
          </ListGroup.Item>
          <ListGroup.Item>
            Issues: <span style={{ float: "right" }}>{issues}</span>
          </ListGroup.Item>
          <ListGroup.Item>
            Unit Tests: <span style={{ float: "right" }}>{unit_tests}</span>
          </ListGroup.Item>
        </ListGroup>
      </Card.Body>
    </Card>
  );
}

export default function About() {
  const isInitialRender = useRef(true);

  const [commits, setCommits] = useState([]);
  const [issues, setIssues] = useState([]);
  let totalIssues = 0;
  let totalCommits = 0;
  let totalUnitTests = 0;

  const projectId = process.env.REACT_APP_PROJECT_ID;
  const gitLabToken = process.env.REACT_APP_GITLAB_TOKEN;

  useEffect(() => {
    if (isInitialRender.current) {
      isInitialRender.current = false;
      const fetchCommits = async () => {
        const commitsPerPage = 100;
        let allCommits = [];
        let page = 1;
        let nextPage = true;

        while (nextPage) {
          const response = await axios.get(
            `https://gitlab.com/api/v4/projects/${projectId}/repository/commits`,
            {
              params: {
                per_page: commitsPerPage,
                page: page,
                all: true,
              },
              headers: {
                "PRIVATE-TOKEN": gitLabToken,
              },
            }
          );

          const commits = response.data;
          allCommits = allCommits.concat(commits);
          if (response.headers["x-next-page"] != "") {
            page++;
          } else {
            nextPage = false;
          }
        }
        setCommits(allCommits);
      };
      fetchCommits();
      axios
        .get(`https://gitlab.com/api/v4/projects/${projectId}/issues`, {
          params: {
            per_page: 100,
            all: true,
          },
          headers: {
            "PRIVATE-TOKEN": gitLabToken,
          },
        })
        .then((response) => {
          setIssues(response.data);
        });
    }
  }, []);

  function countCommits(data, name) {
    if (!data) return;
    let counter = 0;

    data.forEach((i, index) => {
      if (name.includes(i.author_name)) {
        counter++;
      }
    });
    return counter;
  }
  function countIssues(data, name) {
    if (!data) return;
    let counter = 0;

    data.forEach((i, index) => {
      if (i.closed_by && i.closed_by.length != 0) {
        if (name.includes(i.closed_by.name)) {
          counter++;
        }
      }
    });
    return counter;
  }

  return (
    <Container className="py-5  ">
      <Container className="pt-5 text-center" style={{ maxWidth: "1000px" }}>
        <p className="display-4 pb-5 accent">About Us</p>
        <p className="lead text-center" style={{ fontSize: "1.5rem" }}>
          In Japan, earthquakes are frequent occurrences, posing significant
          risks due to its proximity to the Ring of Fire—an area prone to
          volcanic eruptions and seismic activity caused by plate tectonics.
          Unshakable Japan serves as a vital resource offering insights into
          past earthquakes, their impact on Japanese prefectures, and the
          available support for earthquake victims.
        </p>
        <p className="lead pb-5 text-center" style={{ fontSize: "1.5rem" }}>
          By using disparate data sources, we provide users with a holistic
          understanding of earthquakes in Japan and equip them with knowledge to
          stay safe and prepared in the event of a seismic event.
        </p>
      </Container>

      <Container className="pb-5 text-center">
        <h1 className="display-4 pb-5 accent ">Meet the Team</h1>

        <div className="d-flex flex-wrap justify-content-center gap-5">
          {aboutCardData.map((member, index) => {
            let _commits = countCommits(commits, member.name);
            let _issues = countIssues(issues, member.name);
            totalIssues += _issues;
            totalCommits += _commits;
            totalUnitTests += member.unit_tests;
            return (
              <AboutCard
                key={index}
                img={member.img}
                name={member.name}
                bio={member.bio}
                role={member.role}
                gitlab_email={member.gitlab_email}
                commits={_commits}
                issues={_issues}
                unit_tests={member.unit_tests}
              />
            );
          })}
        </div>
      </Container>
      <Container className="pb-5 text-center">
        <p className="display-4 pb-2 accent">Totals</p>
        <Col md={4} className="m-auto ">
          <ListGroup className="h4">
            <ListGroup.Item>
              Total Commits:{" "}
              <span style={{ float: "right" }}>{totalCommits}</span>
            </ListGroup.Item>
            <ListGroup.Item>
              Total Issues:{" "}
              <span style={{ float: "right" }}>{totalIssues}</span>
            </ListGroup.Item>
            <ListGroup.Item>
              Total Unit Tests:{" "}
              <span style={{ float: "right" }}>{totalUnitTests}</span>
            </ListGroup.Item>
          </ListGroup>
        </Col>
      </Container>
      <Container className="text-center pb-5">
        <p className="display-4 pb-2 accent">Data Sources</p>

        {dataSources.map((source, index) => (
          <a
            key={index}
            href={source.link}
            target="_blank"
            rel="noreferrer"
            className="text-decoration-none"
          >
            <p>{source.name}</p>
          </a>
        ))}
      </Container>
      <Container className="text-center pb-5">
        <p className="display-4 pb-2 accent">Tools Used</p>
        <Row className=" justify-content-center gap-2">
          {toolCardData.map((tool, index) => (
            <Card
              key={index}
              style={{ width: "10rem" }}
              className="text-center pt-3"
            >
              <div className="d-flex justify-content-center">
                <Card.Img
                  style={{ width: "100px" }}
                  variant="top"
                  src={require(`../assets/tools/${tool.imgName}`)}
                />
              </div>

              <Card.Body>
                <Card.Title>{tool.name}</Card.Title>
              </Card.Body>
            </Card>
          ))}
        </Row>
      </Container>
      <Container className="text-center pb-5">
        <ButtonGroup size="lg" className="gap-2">
          <a
            href="https://documenter.getpostman.com/view/32899457/2sA2r53kKi"
            target="_blank"
            rel="noreferrer"
            className="text-decoration-none"
          >
            <Button>API Documentation</Button>
          </a>

          <a
            href="https://gitlab.com/tarun.ajjarapu/cs373-group-4"
            target="_blank"
            rel="noreferrer"
            className="text-decoration-none"
          >
            <Button>GitLab Repository</Button>
          </a>
        </ButtonGroup>
      </Container>
    </Container>
  );
}
