import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Container from "react-bootstrap/Container";
import createMapsURL from "../services/googleMaps";
import { EarthquakeCard } from "./Earthquakes";
import { ResourceCard } from "./Resources";
import { getRand, select } from "./EarthquakeInfo";

const fallback = [
  "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Kongobuji_Koyasan07n3200.jpg/220px-Kongobuji_Koyasan07n3200.jpg",
  "https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Chuurei-tou_Fujiyoshida_17025277650_c59733d6ba_o.jpg/220px-Chuurei-tou_Fujiyoshida_17025277650_c59733d6ba_o.jpg",
  "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Regions_and_Prefectures_of_Japan_2.svg/330px-Regions_and_Prefectures_of_Japan_2.svg.png",
];

const PrefInfo = () => {
  const { prefectureId } = useParams();
  const [data, setData] = useState({});
  const [earthquakeData, setEarthquakeData] = useState([]);
  const [resourcesData, setResourcesData] = useState([]);

  async function getWikiImage(name, fallback) {
    fetch(
      `https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles=${name}_Prefecture&origin=*`
    )
      .then((response) => {
        if (!response.ok) {
          throw new Error("Error");
        }
        return response.json();
      })
      .then((data) => {
        if (data.query.pages[Object.keys(data.query.pages)[0]].original) {
          setData((prevData) => {
            return {
              ...prevData,
              image:
                data.query.pages[Object.keys(data.query.pages)[0]].original
                  .source,
            };
          });
        } else {
          setData((prevData) => {
            return {
              ...prevData,
              image: getRand(fallback, 1),
            };
          });
        }
      })
      .catch((error) => {
        console.error("Error", error);
      });
  }
  useEffect(() => {
    fetch(`https://api.unshakablejapan.me/prefectures/${prefectureId}`)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then(async (data) => {
        setData(data);
        setResourcesData(getRand(data.resources, 3));
        setEarthquakeData(data.earthquakes);
        await getWikiImage(data.name, fallback);
      });
  }, []);

  useEffect(() => {
    if ("earthquakes" in data) {
      const fetchData = async () => {
        const fetchPromises = data.earthquakes.map((earthquake) =>
          fetch(
            `https://api.unshakablejapan.me/earthquakes/${earthquake.id}`
          ).then((response) => {
            return response.json();
          })
        );
        try {
          const earthquakesData = await Promise.all(fetchPromises);
          setEarthquakeData(earthquakesData);
        } catch (error) {
          console.error("Error", error);
        }
      };

      fetchData();
    }
  }, [data]);
  useEffect(() => {
    if (Object.keys(data).length !== 0) {
      const resourceIds = select(data.resources, prefectureId);
      const fetchData = async () => {
        const fetchPromises = resourceIds.map((resourceId) =>
          fetch(
            `https://api.unshakablejapan.me/resources/${resourceId.id}`
          ).then((response) => {
            if (response.ok) {
              return response.json();
            }
          })
        );

        try {
          const resourcesData = await Promise.all(fetchPromises);
          setResourcesData(resourcesData);
        } catch (error) {
          console.error("Error", error);
        }
      };

      fetchData();
    }
  }, [data]);
  return (
    <Container className="py-5 ">
      <h2 className="pt-5 display-2 font-weight-bold text-dark">{data.name}</h2>
      <div className="d-flex flex-wrap justify-content-between">
        <div>
          <p className=" lead">Island: {data.island}</p>
          <p className=" lead">
            Population: {data.population && data.population.toLocaleString()}
          </p>
          <p className=" display-6">Risk: {data.risk}%</p>
        </div>
        <img className="img-fluid" src={data.image} alt="" width={500} />
      </div>
      <div className="py-5 d-flex flex-wrap gap-2 justify-content-center align-items-start">
        {data.longitude && data.latitude && (
          <iframe
            title="Embedded Map"
            src={createMapsURL(data.latitude, data.longitude, 7)}
            width="100%"
            height="400px"
            border="0"
            allowFullScreen=""
            aria-hidden="false"
            tabIndex="0"
          />
        )}
      </div>

      <div className="d-flex flex-wrap gap-4  justify-content-center align-items-start text-center">
        <div className="text-center ">
          <p>Related Earthquake</p>
          {earthquakeData.length !== 0 && (
            <div className="d-flex flex-wrap justify-content-center gap-2">
              {earthquakeData.map((earthquakeProps, index) => (
                <EarthquakeCard key={index} {...earthquakeProps} />
              ))}
            </div>
          )}
        </div>
        <div className="text-center d-flex flex-column ">
          <p>Related Resources</p>
          {resourcesData.length !== 0 && (
            <div className="d-flex flex-wrap justify-content-center gap-2">
              {resourcesData.map((resourceProps, index) => {
                return resourceProps.languages ? (
                  <ResourceCard key={index} highlight={""} {...resourceProps} />
                ) : null;
              })}
            </div>
          )}
        </div>
      </div>
    </Container>
  );
};

export default PrefInfo;
