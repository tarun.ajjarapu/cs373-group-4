import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Button
} from "react-bootstrap";
import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";
import SearchBar from "../components/SearchBar";
import { PaginationComponent } from "../components/PaginationComponent";
import { useParams } from "react-router-dom";
import { highlightMatch } from "./Prefectures";

const ResourceCard = ({
  id,
  name,
  info_type,
  image,
  languages,
  link,
  percent_coverage,
  disaster_types,
  highlight,
}) => (
  <Card style={{ width: "20rem", height: "31rem" }}>
    <Card.Img src={image}></Card.Img>

    <Card.Body className="d-flex flex-column justify-content-evenly">
      <Link to={link} target="_blank">
        <Card.Title>{highlightMatch(name, highlight)}</Card.Title>
      </Link>
      <Card.Text>Type: {info_type}</Card.Text>
      <Card.Text>Languages Supported: {languages.split("#").length}</Card.Text>
      <Card.Text>
        Disaster Types Supported: {disaster_types.split("#").length}
      </Card.Text>
      <Card.Text>Percent Coverage: {percent_coverage}%</Card.Text>
    </Card.Body>
    <Card.Footer>
      <Link to={`/Resources/${id}`}>
        <Button variant="primary">More Info</Button>
      </Link>
    </Card.Footer>
  </Card>
);

const PrefectureCard = ({
  name,
  island,
  population,
  risk,
  density,
  id,
  highlight,
}) => (
  <Card style={{ width: "20rem", height: "20rem" }}>
    <Card.Body className="d-flex flex-column justify-content-evenly">
      <Card.Title>{highlightMatch(name, highlight)}</Card.Title>
      <div className="">
        <Card.Text>Island: {island}</Card.Text>
        <Card.Text>Population: {population.toLocaleString()}</Card.Text>
        <Card.Text>Density: {density}</Card.Text>
        <Card.Text>Risk Score: {risk}%</Card.Text>
      </div>
    </Card.Body>
    <Card.Footer>
      <Link to={`/prefectures/${id}`}>
        <Button variant="primary">More Info</Button>
      </Link>
    </Card.Footer>
  </Card>
);

const EarthquakeCard = ({
  name,
  date,
  magnitude,
  casualties,
  year,
  id,
  highlight,
}) => (
  <Card style={{ width: "20rem", height: "20rem" }}>
    <Card.Body className="d-flex flex-column justify-content-evenly">
      <Card.Title>{highlightMatch(name, highlight)}</Card.Title>
      <div className="">
        <Card.Text>Date: {date}</Card.Text>
        <Card.Text>Year: {year}</Card.Text>
        <Card.Text>Magnitude: {magnitude}</Card.Text>
        <Card.Text>People Affected: {casualties}</Card.Text>
      </div>
    </Card.Body>
    <Card.Footer>
      <Link to={`/earthquakes/${id}`}>
        <Button variant="primary">More Info</Button>
      </Link>
    </Card.Footer>
  </Card>
);

const determineCardComponent = (item, index, highlight) => {
  switch (item.model) {
    case "resource":
      return <ResourceCard key={index} {...item} highlight={highlight} />;
    case "prefecture":
      return <PrefectureCard key={index} {...item} highlight={highlight} />;
    case "earthquake":
      return <EarthquakeCard key={index} {...item} highlight={highlight} />;
    default:
      return null;
  }
};

const Resources = () => {
  const [data, setData] = useState([]);
  const [pageData, setPageData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [highlight, setHighlight] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const itemsPerPage = 12;
  const totalPages = Math.ceil(data.length / itemsPerPage);
  const { query } = useParams();

  useEffect(() => {
    if (query) {
      setIsLoading(true);
      setHighlight(query);

      fetch(`https://api.unshakablejapan.me/search?query=${query}`)
        .then((response) => response.json())
        .then((searchData) => {
          setData(searchData);
          setCurrentPage(1);
          setIsLoading(false);
        });
    } else {

      setHighlight("");
      setData([]);
    }
  }, [query]);

  useEffect(() => {
    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    setPageData(data.slice(indexOfFirstItem, indexOfLastItem));
  }, [data, currentPage]);

  const onSubmit = (query) => {
    if (query) {
      setHighlight(query);
      setIsLoading(true);
      fetch(`https://api.unshakablejapan.me/search?query=${query}`)
        .then((response) => response.json())
        .then((searchData) => {
          setData(searchData);
          setCurrentPage(1);
          setIsLoading(false);
        });
    } else {
      setHighlight("");
      setData([]);
    }
  };

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  console.log(pageData);

  return (
    <Container className="py-5 text-center">
      <h1 className="display-4 py-4">Search All</h1>
      <SearchBar onSubmit={onSubmit} />
      {error && <p className="text-danger">{error}</p>}
      {!isLoading && (
        <div>
          <Row className="d-flex justify-content-center gap-4 py-4">
            {pageData.map((item, index) =>
              determineCardComponent(item, index, highlight)
            )}
          </Row>
          <PaginationComponent
            data={data}
            currentPage={currentPage}
            totalPages={totalPages}
            handlePageChange={handlePageChange}
          />
        </div>
      )}
      <div className="py-4">
        {isLoading ? (
          <p>Loading data...</p>
        ) : data.length === 0 ? (
          <p>No Earthquakes, Prefectures, or Resources found</p>
        ) : (
          <p>Number of Instances: {data.length}</p>
        )}
      </div>
    </Container>
  );
};

export default Resources;
