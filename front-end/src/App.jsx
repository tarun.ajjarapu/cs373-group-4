import "bootstrap/dist/css/bootstrap.min.css";

import Home from "./pages/Home";
import Earthquakes from "./pages/Earthquakes";
import Prefectures from "./pages/Prefectures";
import Resources from "./pages/Resources";
import About from "./pages/About";
import AllSearch from "./pages/AllSearch";
import PrefInfo from "./pages/PrefectureInfo";
import EarthInfo from "./pages/EarthquakeInfo";
import ResourceInfo from "./pages/ResourceInfo";

import "./App.css";

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import NavBar from "./components/NavBar";
import Visualizations from "./pages/Visualizations";
import ProviderVisualizations from "./pages/ProviderVisualizations";

const App = () => {
  return (
    <Router>
      <div className="App ">
        <NavBar />

        <Routes>
          <Route path="/" Component={Home} />
          <Route path="/earthquakes" Component={Earthquakes} />
          <Route path="/earthquakes/:earthquakeId" Component={EarthInfo} />
          <Route path="/prefectures" Component={Prefectures} />
          <Route path="/prefectures/:prefectureId" Component={PrefInfo} />
          <Route path="/resources" Component={Resources} />
          <Route path="/resources/:resourceId" Component={ResourceInfo} />
          <Route path="/allSearch/:query" Component={AllSearch} />
          <Route path="/about" Component={About} />
          <Route path="/visualizations" Component={Visualizations} />
          <Route
            path="/provider-visualizations"
            Component={ProviderVisualizations}
          />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
