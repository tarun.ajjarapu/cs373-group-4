const createMapsURL = (
  latitude,
  longitude,
  zoomLevel = 15,
  maptype = "roadmap"
) => {
  if (latitude && longitude) {
    return `https://www.google.com/maps/embed/v1/place?key=${process.env.REACT_APP_MAP_KEY}&q=${latitude},${longitude}&zoom=${zoomLevel}&maptype=${maptype}`;
  }
  return "";
};

export default createMapsURL;
