from sqlalchemy import or_
from models import Prefecture, Earthquake, Resource
from models import PrefectureSchemaClean, EarthquakeSchema, ResourceSchema, ResourceSchemaClean

# contains logic for searching the website

def calculate_relevance(query, dict):
    name = dict['name'].lower()
    result_text = "".join(str(value) for value in dict.values()).lower()
    if not query:
        return 0
    terms = query.split()
    score = 0
    full_query = " ".join(terms).lower()
    # extremely high relevance if the full search query is in the name of the object
    if name.startswith(full_query):
        # number of points is based on how close the name and the query are in length
        score += max(20 - (len(name) - len(full_query)), 6)
    for term in terms:
        # high relevance if individual term is in the name
        if term in name:
            score += 4
        # still relevant if indivdual term is in the rest of the data
        elif term in result_text:
            score += 2
    return score


def get_relevance_list(model, modelSchema, query):
    objects = []
    if query == "":
        objects = model.query.all()
    else:
        filters = create_filter(model, query)
        objects = model.query.filter(or_(*filters)).all()
    
    objects_with_ratings = []
    if model == Resource:
        resource_list = add_coverage_to_resources(objects)
        objects_with_ratings = [(calculate_relevance(query, ResourceSchemaClean().dump(resource)), resource) for resource in resource_list]
    else:
        for object in objects:
            schema = modelSchema()            
            result = schema.dump(object)
            if model == Prefecture:
                result['model'] = 'prefecture'
            else:
                result['model'] = 'earthquake'
            object_with_rating = (calculate_relevance(query, modelSchema().dump(object)), result)
            objects_with_ratings.append(object_with_rating)
    return objects_with_ratings


def create_filter(model, query):
    terms = query.split()
    filters = []
    if model == Prefecture:
        for term in terms:
            filters.extend(
                [
                    Prefecture.name.ilike(f"%{term}%"),
                    Prefecture.capital_city.ilike(f"%{term}%"),
                    Prefecture.island.ilike(f"%{term}%")
                ]
            )
    elif model == Earthquake:
        for term in terms:
            filters.extend(
                [
                    Earthquake.name.ilike(f"%{term}%"),
                    Earthquake.date.ilike(f"%{term}%"),
                    Earthquake.year.ilike(f"%{term}%"),
                ]
            )
    else:
        for term in terms:
            filters.extend(
                [
                    Resource.name.ilike(f"%{term}%"),
                    Resource.languages.ilike(f"%{term}%"),
                    Resource.disaster_types.ilike(f"%{term}%"),
                    Resource.info_type.ilike(f"%{term}%"),
                ]
            )
    return filters



def search(model, modelSchema, query):
    object_list_with_rating = []
    if model == None or modelSchema == None:
        object_list_with_rating.extend(get_relevance_list(Prefecture, PrefectureSchemaClean, query))
        object_list_with_rating.extend(get_relevance_list(Earthquake, EarthquakeSchema, query))
        object_list_with_rating.extend(get_relevance_list(Resource, ResourceSchema, query))
    else:
        object_list_with_rating.extend(get_relevance_list(model, modelSchema, query))
    sorted_list = sorted(object_list_with_rating, key=lambda x: x[0], reverse=True)
    final_list = [item for relevance, item in sorted_list]
    return final_list


def calculate_coverage(resource):
    obj = ResourceSchema().dump(resource)
    prefecture_list = obj['prefectures']
    num_prefectures = len(prefecture_list)
    return round((100*num_prefectures/47), 2)


def add_coverage_to_resources(resources):
    resources_serialized = []
    for resource in resources:
        result = ResourceSchemaClean().dump(resource)
        result['percent_coverage'] = calculate_coverage(resource)
        result['model'] = 'resource'
        resources_serialized.append(result)
    return resources_serialized
