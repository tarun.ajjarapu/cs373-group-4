# a place to define database models
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields

db = SQLAlchemy()

prefecture_resource_association = db.Table(
    'prefecture_resource_association',
    db.Column('prefecture', db.ForeignKey('prefecture.id'), primary_key=True),
    db.Column('resource', db.ForeignKey('resource.id'), primary_key=True)
)  

class Prefecture(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255))
    population = db.Column(db.Integer)
    area = db.Column(db.Float)
    density = db.Column(db.Float)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    risk = db.Column(db.Float)
    capital_city = db.Column(db.String(255))
    island = db.Column(db.String(255))
    num_districts = db.Column(db.Integer)
    num_municipalities = db.Column(db.Integer)
    iso = db.Column(db.String(255))
    area_code = db.Column(db.String(255))
    resources = db.relationship('Resource', secondary=prefecture_resource_association, back_populates='prefectures')
    earthquakes = db.relationship('Earthquake', back_populates='prefecture')

class PrefectureSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    population = fields.Int()
    area = fields.Float()
    density = fields.Float()
    latitude = fields.Float()
    longitude = fields.Float()
    risk = fields.Float()
    capital_city = fields.Str()
    island = fields.Str()
    num_districts = fields.Int()
    num_municipalities = fields.Int()
    iso = fields.Str()
    area_code = fields.Str()
    resources = fields.Nested(lambda: ResourceSchema(only=("id", "name")), many=True)
    earthquakes = fields.Nested(lambda: EarthquakeSchema(only=("id", "name", "year")), many=True)

class PrefectureSchemaClean(Schema):
    id = fields.Int()
    name = fields.Str()
    population = fields.Int()
    area = fields.Float()
    density = fields.Float()
    latitude = fields.Float()
    longitude = fields.Float()
    risk = fields.Float()
    capital_city = fields.Str()
    island = fields.Str()
    num_districts = fields.Int()
    num_municipalities = fields.Int()
    iso = fields.Str()
    area_code = fields.Str()

class Earthquake(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255))
    prefecture_id = db.Column(db.Integer, db.ForeignKey('prefecture.id'))
    magnitude = db.Column(db.Float)
    date = db.Column(db.String(255))
    year = db.Column(db.Integer)
    casualties = db.Column(db.Integer)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    prefecture = db.relationship('Prefecture', back_populates='earthquakes')

class EarthquakeSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    prefecture_id = fields.Int()
    magnitude = fields.Float()
    date = fields.Str()
    year = fields.Int()
    casualties = fields.Int()
    latitude = fields.Float()
    longitude = fields.Float()

class Resource(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255))
    link = db.Column(db.String(255))
    languages = db.Column(db.String(255))
    disaster_types = db.Column(db.String(255))
    info_type = db.Column(db.String(255))
    image = db.Column(db.String(255))
    prefectures = db.relationship('Prefecture', secondary=prefecture_resource_association, back_populates='resources')


class ResourceSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    link = fields.Str()
    languages = fields.Str()
    disaster_types = fields.Str()
    info_type = fields.Str()
    image = fields.Str()
    prefectures = fields.Nested(PrefectureSchema(only=("id", "name")), many=True)


class ResourceSchemaClean(Schema):
    id = fields.Int()
    name = fields.Str()
    link = fields.Str()
    languages = fields.Str()
    disaster_types = fields.Str()
    info_type = fields.Str()
    image = fields.Str()

