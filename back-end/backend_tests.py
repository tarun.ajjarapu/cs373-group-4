import unittest
import app
import requests
import json

app.app.config["TESTING"] = True
client = app.app.test_client()
url = "https://www.unshakablejapan.me"

class Tests(unittest.TestCase):
    def test_perfectures_general(self):
        response = requests.get(url + "/prefectures")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn('prefectures', data)

    def test_earthquake_general(self):
        response = requests.get(url + "/earthquakes")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn('earthquakes', data)

    def test_resource_general(self):
        response = requests.get(url + "/resources")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn('resources', data)
    
    def test_prefecture_specifc(self):
        response = requests.get(url + "/prefectures/23")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn('Aichi', data)

    def test_earthquake_specific(self):
        response = requests.get(url + "/earthquakes/89")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn('1293 Kamakura earthquake', data)

    def test_resource_specific(self):
        response = requests.get(url + "/resources/40")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn('Airport and Airline Status', data)

    def test_search_1(self): # Change whenever implemented
        response = requests.get(url + "/prefectures/?search=aichi")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn('Aichi', data)

    def test_search_2(self):
        response = requests.get(url + "/earthquakes/?search=aizu")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn('1611 Aizu earthquake', data)

    def test_search_3(self):
        response = requests.get(url + "/resources/?search=National")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn('National Research Institute for Earth Science and Disaster Resilience', data)

    def test_sorting_1(self):
        response = requests.get(url + "/prefectures?sortby=island")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn('Hokkaido', data)

    def test_sorting_2(self):
        response = requests.get(url + "/earthquakes?sortby=magnitude")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn('Mobara earthquake', data)

    def test_sorting_3(self):
        response = requests.get(url + "/resources?sortby=percentcoverage")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn('NHK NEWS WEB', data)

    def test_filter_1(self):
        response = requests.get(url + "/prefectures?search=Shikoku")
        try:
            data = response.json()
        except json.decoder.JSONDecodeError:
            data = None

        self.assertEqual(response.status_code, 200) #200 indicates success
        if data is not None:
            self.assertIn("Ehime", data)
   



if __name__ == "__main__":
    unittest.main()