from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from dotenv import load_dotenv
import os
from models import db
from models import Prefecture, Earthquake, Resource
from models import PrefectureSchema, EarthquakeSchema, ResourceSchema
from models import PrefectureSchemaClean, ResourceSchemaClean
from searching import calculate_relevance, search, add_coverage_to_resources, calculate_coverage

load_dotenv()

app = Flask(__name__)
CORS(app)

app.config["SQLALCHEMY_DATABASE_URI"] = (
    "mysql://"
    #+ os.environ.get("DB_USERNAME", "aaaa")
    + os.getenv("DB_USERNAME", "default_username")
    + ":"
    #+ os.environ.get("DB_PASSWORD", "aaaa")
    + os.getenv("DB_PASSWORD", "default_password")
    + "@"
    #+ os.environ.get("DB_HOST", "aaaa")
    + os.getenv("DB_HOST", "localhost")
    + ":3306/"
    #+ os.environ.get("DB_NAME", "aaaa")
    + os.getenv("DB_NAME", "default_database")
)
app.config["SQLALCHEMY_TRACK_MODIFICATIIONS"] = False

db.init_app(app)


@app.route("/")
def home():
    return """
    <h1>Welcome to Unshakable Japan</h1>
    <ul>
        <li><a href="/earthquakes">Earthquakes</a></li>
        <li><a href="/prefectures">Prefectures</a></li>
        <li><a href="/resources">Resources</a></li>
    </ul>
    """


@app.route("/prefectures", methods=["GET"])
def get_all_prefectures():
    search_query = request.args.get('query')
    sort_by = request.args.get('sort')
    filter_by = request.args.get('Island')
    prefectures = Prefecture.query.order_by(getattr(Prefecture, 'name'))
    if search_query:
        return jsonify(search(Prefecture, PrefectureSchemaClean, search_query))
    elif sort_by:
        if hasattr(Prefecture, sort_by):
            prefectures = Prefecture.query.order_by(getattr(Prefecture, sort_by))
        else:
            return jsonify('error: invalid sort parameter')
    elif filter_by:
        filter_by_island = filter_by.capitalize()
        prefectures = prefectures.filter(Prefecture.island.like(f"{filter_by_island}"))
    prefectures = prefectures.all()
    prefectures_serialized = [PrefectureSchemaClean().dump(prefecture) for prefecture in prefectures]
    return jsonify(prefectures_serialized)


@app.route("/prefectures/<id>", methods=["GET"])
def get_prefecture(id):
    prefecture = Prefecture.query.get(id)
    if prefecture:
        schema = PrefectureSchema()
        result = schema.dump(prefecture)
        return jsonify(result)
    return jsonify({"Error": "Prefecture not found"}), 404


@app.route("/earthquakes", methods=["GET"])
def get_all_earthquakes():
    search_query = request.args.get('query')
    sort_by = request.args.get('sort')
    filter_by = request.args.get('Date')
    earthquakes = Earthquake.query.order_by(getattr(Earthquake, 'name'))
    if search_query:
        return jsonify(search(Earthquake, EarthquakeSchema, search_query))
    elif sort_by:
        if hasattr(Earthquake, sort_by):
            earthquakes = Earthquake.query.order_by(getattr(Earthquake, sort_by))
        else:
            return jsonify('error: invalid sort parameter')
    elif filter_by:
        filter_by_month = filter_by.capitalize()
        earthquakes = earthquakes.filter(Earthquake.date.like(f"{filter_by_month}%"))
    earthquakes = earthquakes.all()
    earthquakes_serialized = [EarthquakeSchema().dump(earthquake) for earthquake in earthquakes]
    return jsonify(earthquakes_serialized)


@app.route("/earthquakes/<id>", methods=["GET"])
def get_earthquake(id):
    earthquake = Earthquake.query.get(id)
    if earthquake:
        schema = EarthquakeSchema()
        result = schema.dump(earthquake)
        return jsonify(result)
    return jsonify({"Error": "Earthquake not found"}), 404


@app.route("/resources", methods=["GET"])
def get_all_resources():
    search_query = request.args.get('query')
    sort_by = request.args.get('sort')
    filter_type = request.args.get('Type')
    filter_languages = request.args.get('Languages')
    filter_disasters = request.args.get('Disaster Types')
    resources = Resource.query.order_by(getattr(Resource, 'name'))
    if search_query:
        return jsonify(search(Resource, ResourceSchemaClean, search_query))
    elif sort_by and not sort_by == 'Percent Coverage':
        if hasattr(Resource, sort_by):
            resources = Resource.query.order_by(getattr(Resource, sort_by))
        else:
            return jsonify('error: invalid sort parameter')
    elif filter_type:
        resources = resources.filter(Resource.info_type.ilike(f"%{filter_type}%"))
    elif filter_languages:
        resources = resources.filter(Resource.languages.ilike(f"%{filter_languages}%"))
    elif filter_disasters:
        resources = resources.filter(Resource.disaster_types.ilike(f"%{filter_disasters}%"))
    resources = resources.all()
    resources_serialized = add_coverage_to_resources(resources)
    if sort_by and sort_by == 'Percent Coverage':
        resources_serialized = sorted(resources_serialized,  key=lambda x: x['percent_coverage'])
    return jsonify(resources_serialized)


@app.route("/resources/<id>", methods=["GET"])
def get_resource(id):
    resource = Resource.query.get(id)
    if resource:
        schema = ResourceSchema()
        result = schema.dump(resource)
        result['percent_coverage'] = calculate_coverage(resource)
        return jsonify(result)
    return jsonify({"Error": "Resource not found"}), 404


@app.route("/search", methods=["GET"])
def search_all():
    search_query = request.args.get('query')
    if search_query:
        return jsonify(search(None, None, search_query))
    return jsonify({'error:': 'no query'})


@app.route("/test")
def test():
    return "The Unshakable Japan backend server is accessible."

#assisted by https://gitlab.com/sujithaseenivasan/cs373-group-08/-/blob/Develop/back-end/endpoints.py?ref_type=heads
@app.route("/prefectures/visualize")
def visualize_prefectures():
    session = DBsession()
    query = session.query(Prefecture)
    ret_list = {}
    islandfilter = query.filter(func.lower(Prefecture.island).contains(func.lower("hokkaido")))
    ret_list['Hokkaido'] = islandfilter.count()
    islandfilter = query.filter(func.lower(Prefecture.island).contains(func.lower("honshu")))
    ret_list['Honshu'] = islandfilter.count()
    islandfilter = query.filter(func.lower(Prefecture.island).contains(func.lower("kyushu")))
    ret_list['Kyushu'] = islandfilter.count()
    islandfilter = query.filter(func.lower(Prefecture.island).contains(func.lower("shikoku")))
    ret_list['Shikoku'] = islandfilter.count()
    islandfilter = query.filter(func.lower(Prefecture.island).contains(func.lower("ryukyu islands")))
    ret_list['Ryukyu Islands'] = islandfilter.count()
    otherInstances = query.filter(~(
        func.lower(Prefecture.island).contains(func.lower("hokkaido")) |
        func.lower(Prefecture.island).contains(func.lower("honshu")) |
        func.lower(Prefecture.island).contains(func.lower("kyushu")) |
        func.lower(Prefecture.island).contains(func.lower("shikoku")) |
        func.lower(Prefecture.island).contains(func.lower("ryukyu islands"))
    ))
    ret_list['Other'] = otherInstances.count()
    print(ret_list)
    return ret_list


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
