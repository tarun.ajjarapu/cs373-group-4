TO BUILD AND RUN

cd back-end
docker build -t backend .
docker run -p 8080:8080 backend

backend should now be hosted on localhost:8080

TO ADD OBJECTS TO MYSQL TO TEST

cd back-end
docker build -t backend .
docker run -d -p 8080:8080 backend
(this will output a container id)
docker exec -it <container_id> bash
In the container shell:
python upload_test.py

exit
docker stop <container_id>