import requests
from bs4 import BeautifulSoup
import re
import requests
from dotenv import load_dotenv
import os
from app import app
from models import db, Prefecture, Earthquake, Resource

original = {"Aichi": 0, "Akita": 1, "Aomori": 2, "Chiba": 3, "Ehime": 4, "Fukui": 5, "Fukuoka": 6, "Fukushima": 7, "Gifu": 8, "Gunma": 9, "Hiroshima": 10, "Hokkaido": 11, "Hyōgo": 12, "Ibaraki": 13, "Ishikawa": 14, "Iwate": 15, "Kagawa": 16, "Kagoshima": 17, "Kanagawa": 18, "Kōchi": 19, "Kumamoto": 20, "Kyōto": 21, "Mie": 22, "Miyagi": 23, "Miyazaki": 24, "Nagano": 25, "Nagasaki": 26, "Nara": 27, "Niigata": 28, "Ōita": 29, "Okayama": 30, "Okinawa": 31, "Ōsaka": 32, "Saga": 33, "Saitama": 34, "Shiga": 35, "Shimane": 36, "Shizuoka": 37, "Tochigi": 38, "Tokushima": 39, "Tōkyō": 40, "Tottori": 41, "Toyama": 42, "Wakayama": 43, "Yamagata": 44, "Yamaguchi": 45, "Yamanashi": 46}
newPrefecture = ["Hokkaido", "Aomori", "Iwate", "Miyagi", "Akita", "Yamagata", "Fukushima", "Ibaraki", "Tochigi", "Gunma", "Saitama", "Chiba", "Tokyo", "Kanagawa", "Niigata", "Toyama", "Ishikawa", "Fukui", "Yamanashi", "Nagano", "Gifu", "Shizuoka", "Aichi", "Mie", "Shiga", "Kyoto", "Osaka", "Hyogo", "Nara", "Wakayama", "Tottori", "Shimane", "Okayama", "Hiroshima", "Yamaguchi", "Tokushima", "Kagawa", "Ehime", "Kochi", "Fukuoka", "Saga", "Nagasaki", "Kumamoto", "Oita", "Miyazaki", "Kagoshima", "Okinawa"]

newPrefecture.sort()

curr = requests.get("https://en.wikipedia.org/wiki/Prefectures_of_Japan")
soup = BeautifulSoup(curr.content, 'html.parser')
table = soup.find('table', class_='wikitable sortable')
japan_data = []

for row in table.find_all('tr')[1:]:
    col = row.find_all('td')
    if col:
        prefecture_link = col[0].find('a', href=True)
        prefecture_url = 'https://en.wikipedia.org' + prefecture_link['href']
        latitude, longitude = None, None
        if prefecture_url:
            prefecture_curr = requests.get(prefecture_url)
            prefecture_soup = BeautifulSoup(prefecture_curr.content, 'html.parser')
            geo = prefecture_soup.find(class_='geo-default')
            if geo:
                latitude_span = geo.find(class_='latitude')
                longitude_span = geo.find(class_='longitude')
                latitude = latitude_span.text
                longitude = longitude_span.text
        japan_data.append({
            'name': newPrefecture[original[col[0]]],
            'population': int(col[6]),
            'area': float(col[7]),
            'region': col[4],
            'density': float(col[8]),
            'latitude': float(latitude),
            'longitude': float(longitude),
            'capital_city': col[2],
            'island': col[5],
            'num_districts': int(col[9]),
            'num_municipalities': int(col[10]),
            'iso': col[11],
            'area_code': col[12]
        })

curr = requests.get("https://resources.realestate.co.jp/living/2020-japan-national-seismic-map-check-the-probability-of-an-earthquake-occurring-where-you-live/")
soup = BeautifulSoup(curr.content, 'html.parser')
tables = soup.find_all('table')
table = tables[1]
earthquake_data = []

for row in table.find_all('tr')[2:]:
    col = row.find_all('td')
    if col:
        if col[2].text != '':
            temp = col[1].text.strip()
            if temp == '':
                temp = 'Tokyo'
            earthquake_data.append({
                'Prefecture': temp,
                'earthquake_probability': float(col[2])
            })
combined_data = []
for japan in japan_data:
    combined_entry = japan.copy()
    for earthquake in earthquake_data:
        if japan['name'] == earthquake['Prefecture']:
            combined_entry['risk'] = earthquake['earthquake_probability']
            break
    if 'risk' not in combined_entry:
        combined_entry['risk'] = float(38)
    combined_data.append(combined_entry)
prefecure_full_data = combined_data

load_dotenv()
def get_near(latitude, longitude, radius=50):
    url = f"https://maps.googleapis.com/maps/api/geocode/json?latlng={latitude},{longitude}&key=" + str(os.environ.get("APP_MAP_KEY"))
    response = requests.get(url)
    data = response.json()
    min_distance = float('inf')
    if 'results' in data and len(data['results']) > 0:
        for result in data['results']:
            for component in result['address_components']:
                if 'administrative_area_level_1' in component['types']:
                    prefecture_name = component['long_name']
                    distance = latitude * longitude
                    if distance <= radius and distance < min_distance:
                        min_distance = distance
                        nearest_prefecture = prefecture_name
    return nearest_prefecture

elements = [
    'Toyama', 'Wakayama', 'Ibaraki', 'Yamaguchi', 'Saitama', 'Ehime', 'Tochigi',
    'Gunma', 'Yamanashi', 'Okinawa', 'Aomori', 'Shimane', 'Kagoshima', 'Kagawa',
    'Miyazaki', 'Tokushima', 'Kochi', 'Akita', 'Oita', 'Okayama', 'Saga'
]

url = "https://en.wikipedia.org/wiki/List_of_earthquakes_in_Japan"
curr = requests.get(url)
soup = BeautifulSoup(curr.content, 'html.parser')
table = soup.find('table', class_='wikitable sortable')
japan_data = []
prev_year = 0
index = 0

for row in table.find_all('tr')[1:]:
    col = row.find_all('td')
    if col:
        earthquake_link = col[3].find('a', href=True)
        earthquake_url = 'https://en.wikipedia.org' + earthquake_link['href'] if earthquake_link else None
        
        latitude, longitude = None, None
        epicenter = col[6].text.strip()
        if earthquake_url:
            earthquake_curr = requests.get(earthquake_url)
            earthquake_soup = BeautifulSoup(earthquake_curr.content, 'html.parser')
            geo = earthquake_soup.find(class_='geo-default') if earthquake_soup else None
            if geo:
                if geo.find(class_='geo-dec'):
                    geo_dec = geo.find(class_='geo').text.split('; ')
                    latitude, longitude = geo_dec
                else:
                    latitude_span = geo.find(class_='latitude')
                    longitude_span = geo.find(class_='longitude')
                    latitude = latitude_span.text if latitude_span else None
                    longitude = longitude_span.text if longitude_span else None
        if col:
            value = col[1]
            casualties = col[2]
            date_parts = col[0]
            date = ' '.join(date_parts[:2])
            year = None
            if len(date_parts) > 1:
                year = date_parts[2].split(':')
                year = year[0][:4]
                prev_year = year
            else:
                year = prev_year
                date = 'December 1'
            magnitude = value
            if casualties is None:
                casualties = 0
            if latitude is None:
                latitude = 35.0
                longitude = 139.0
            prefecture = get_near(float(latitude), float(longitude))
            japan_data.append({
                'date': date,
                'year': int(year),
                'latitude': float(latitude),
                'longitude': float(longitude),
                'magnitude': float(magnitude),
                'casualties': int(casualties),
                'name': col[3],
                'prefecture': prefecture
            })
earthquake_full_data = japan_data

data_dict = set()
full_list = []

currs = [
    ("https://www.mlit.go.jp/river/bousai/olympic/en/helpful03/index.html", 'Evacuation Info'),
    ("https://www.mlit.go.jp/river/bousai/olympic/en/helpful04/index.html", 'Traffic and Logistics Info'),
    ("https://www.mlit.go.jp/river/bousai/olympic/en/helpful01/index.html", 'Damage Status'),
    ("https://www.mlit.go.jp/river/bousai/olympic/en/helpful02/index.html", 'Weather Status'),
    ("https://www.mlit.go.jp/river/bousai/olympic/en/helpful05/index.html", 'Disaster Victim Assistance Info'),
    ("https://www.mlit.go.jp/river/bousai/olympic/en/helpful06/index.html", 'Safety Status'),
    ("https://www.mlit.go.jp/river/bousai/olympic/en/helpful07/index.html", 'Lifeline Information'),
    ("https://www.mlit.go.jp/river/bousai/olympic/en/helpful08/index.html", 'Region Information')
]

region_to_prefecture = {
    "Chubu": ["Aichi", "Fukui", "Gifu", "Ishikawa", "Nagano", "Niigata", "Shizuoka", "Toyama", "Yamanashi"],
    "Hokkaido": ["Hokkaido"],
    "Tohoku": ["Akita", "Aomori", "Fukushima", "Iwate", "Miyagi", "Yamagata"],
    "Kanto": ["Chiba", "Gunma", "Ibaraki", "Kanagawa", "Saitama", "Tochigi", "Tōkyō"],
    "Shikoku": ["Ehime", "Kagawa", "Kōchi", "Tokushima"],
    "Hokuriku": ["Ishikawa", "Fukui", "Toyama"],
    "Kinki": ["Hyōgo", "Kyōto", "Mie", "Nara", "Ōsaka", "Shiga", "Wakayama"],
    "Chugoku": ["Hiroshima", "Okayama", "Shimane", "Tottori", "Yamaguchi"],
    "Kyushu": ["Fukuoka", "Kagoshima", "Kumamoto", "Miyazaki", "Nagasaki", "Ōita", "Okinawa", "Saga"]
}
original = {"Aichi": 0, "Akita": 1, "Aomori": 2, "Chiba": 3, "Ehime": 4, "Fukui": 5, "Fukuoka": 6, "Fukushima": 7, "Gifu": 8, "Gunma": 9, "Hiroshima": 10, "Hokkaido": 11, "Hyōgo": 12, "Ibaraki": 13, "Ishikawa": 14, "Iwate": 15, "Kagawa": 16, "Kagoshima": 17, "Kanagawa": 18, "Kōchi": 19, "Kumamoto": 20, "Kyōto": 21, "Mie": 22, "Miyagi": 23, "Miyazaki": 24, "Nagano": 25, "Nagasaki": 26, "Nara": 27, "Niigata": 28, "Ōita": 29, "Okayama": 30, "Okinawa": 31, "Ōsaka": 32, "Saga": 33, "Saitama": 34, "Shiga": 35, "Shimane": 36, "Shizuoka": 37, "Tochigi": 38, "Tokushima": 39, "Tōkyō": 40, "Tottori": 41, "Toyama": 42, "Wakayama": 43, "Yamagata": 44, "Yamaguchi": 45, "Yamanashi": 46}
newPrefecture = ["Hokkaido", "Aomori", "Iwate", "Miyagi", "Akita", "Yamagata", "Fukushima", "Ibaraki", "Tochigi", "Gunma", "Saitama", "Chiba", "Tokyo", "Kanagawa", "Niigata", "Toyama", "Ishikawa", "Fukui", "Yamanashi", "Nagano", "Gifu", "Shizuoka", "Aichi", "Mie", "Shiga", "Kyoto", "Osaka", "Hyogo", "Nara", "Wakayama", "Tottori", "Shimane", "Okayama", "Hiroshima", "Yamaguchi", "Tokushima", "Kagawa", "Ehime", "Kochi", "Fukuoka", "Saga", "Nagasaki", "Kumamoto", "Oita", "Miyazaki", "Kagoshima", "Okinawa"]

newPrefecture.sort()

mapCharacters = {'日本語': 'Japanese', 'English': 'English', '한국어': 'Korean', '中文(簡体)': 'Chinese (Traditional)', '中文(繁体)': 'Chinese (Simplified)', 'ประเทศไทย': 'Thai', 'Tiếng Việt': 'Vietnamese', 'Português': 'Portuguese'}

for curr_url, info_type in currs:
    curr = requests.get(curr_url)
    soup = BeautifulSoup(curr.content, 'html.parser')
    items = soup.select('ul.htumbs > li')
    for item in items:
        name = item.find('h3').text
        link = item.find('a')['href']
        img_src = item.find('img')['src']
        languages = '#'.join(mapCharacters[lang.text] for lang in item.select('.langbox li.lan_on'))
        regions = [region.text for region in item.select('.langbox2 li.lan_on')]
        disasters = '#'.join(disaster.text.strip() for disaster in item.select('.bousai-icon li.set_on'))
        if 'Earthquake and Tsunami Disaster' in disasters:
            if name not in data_dict:
                prefectures = []
                for region in regions:
                    prefectures += region_to_prefecture[region]
                prefectures = list(set(prefectures))
                newList = []
                for prefecture in prefectures:
                    newList.append(newPrefecture[original[prefecture]])
                data_dict.add(name)
                full_list.append({
                    'name': name,
                    'link': link,
                    'languages': languages,
                    'disaster_types': disasters,
                    'prefectures': newList,
                    'info_type': info_type,
                    'image': img_src
                })
resource_full_data = full_list

with app.app_context():
    db.drop_all()
    db.create_all()
    list_pref = {}
    for pref in prefecure_full_data:
        list_pref[pref['name']] = Prefecture(
            name = pref['name'],
            population = pref['population'],
            area = pref['area'],
            density = pref['density'],
            latitude = pref['latitude'],
            longitude = pref['longitude'],
            risk = pref['risk'],
            capital_city = pref['capital_city'],
            island = pref['island'],
            num_districts = pref['num_districts'],
            num_municipalities = pref['num_municipalities'],
            iso = pref['iso'],
            area_code = pref['area_code'],
        )
    list_earth = {}
    for earth in earthquake_full_data:
        list_earth[earth['name']] = Earthquake(
            name = earth['name'],
            magnitude = earth['magnitude'],
            date = earth['date'],
            year = earth['year'],
            casualties = earth['casualties'],
            latitude = earth['latitude'],
            longitude = earth['longitude'],
            prefecture = list_pref[earth['prefecture']]
        )
    for res in resource_full_data:
        current_res = Resource(
            name = res['name'],
            link = res['link'],
            languages = res['languages'],
            disaster_types = res['disaster_types'],
            info_type = res['info_type'],
            image = res['image']
        )
        for pref in res['prefectures']:
            list_pref[pref].resources.append(current_res)
        db.session.add(current_res)
    for pref in list_pref.values():
        db.session.add(pref)
    for earth in list_earth.values():
        db.session.add(earth)
    db.session.commit()
