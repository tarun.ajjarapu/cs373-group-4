import requests
from bs4 import BeautifulSoup

url = "https://www.google.com/search?sca_esv=ce0065805545a41c&sca_upv=1&q=japan+earthquake+news&tbm=nws&source=lnms&sa=X&ved=2ahUKEwiszrDphOGEAxUD_skDHWKrDIoQ0pQJegQIEhAB&biw=1536&bih=695&dpr=1.25"
page = requests.get(url)
soup = BeautifulSoup(page.content, 'html.parser')
news_links = soup.find_all('a', href=lambda href: href and 'https://' in href)

count = 0
for article in news_links:
    if 'data-ved' in article.attrs:
        href_value = article['href']
        if count < 3:
            print(href_value[7:])
            count += 1