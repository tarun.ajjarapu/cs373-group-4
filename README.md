# cs373-group-4 
https://gitlab.com/tarun.ajjarapu/cs373-group-4/-/tree/main

website link: https://www.unshakablejapan.me/

API link: https://api.unshakablejapan.me/

API documentation: https://documenter.getpostman.com/view/32899457/2sA2r53kKi

Gitlab Pipeline: https://gitlab.com/tarun.ajjarapu/cs373-group-4/-/pipelines

Presentation: https://www.youtube.com/watch?v=M94nkKajRNk&ab_channel=BenTruong

# Canvas / Slack group number (please check this carefully) : 
Group 4

# Names of team members:
Jose R Quezada, Mohammad S Hakim, Benjamin S Truong, Tarun Ajjarapu, Shivabalan Balathandayuthapani

# Name of the project (alphanumeric, no spaces, max 32 chars; this will also be your URL):
unshakablejapan.me

# The proposed project: The goal of our project 
In Japan, earthquakes are frequent occurrences, posing significant risks due to its proximity to the Ring of Fire—an area prone to volcanic eruptions and seismic activity caused by plate tectonics. The goal of our project is to serve as an accessible resource hub for earthquake relief in Japan.

URLs of at least three data sources that you will programmatically scrape (at least one must be a RESTful API) (be very sure about this):

# Data Sources:
https://www.worlddata.info/asia/japan/earthquakes.php
https://resources.realestate.co.jp/living/earthquakes-in-japan-resources-for-before-during-and-after-an-earthquake-in-japan/
https://en.wikipedia.org/wiki/List_of_earthquakes_in_Japan - RESTful API
Google maps - RESTful API
https://www.mlit.go.jp/river/bousai/olympic/en/helpful03/index.html

# at least three models
Earthquakes
Prefectures
Resources

# an estimate of the number of instances of each model: 3 per each model
Prefectures - 50
Resources - 100
Earthquakes - 150

# describe five of those attributes for each model that you can filter or sort:
Earthquakes
- Date - sortable
- Magnitude - Sortable
- Name of the earthquake
- Fatalities / People affected - Sortable
- Location affected - sortable
- Range - sortable

Prefectures
- All of the Japanese counties/cities/towns
- Size - sortable
- Established date - sortable
- Population - sortable
- Region - filterable
- Density - sortable

Resources
- Region - filterable
- City
- Postal code - sortable
- Name - sortable
- Type - filterable
- Prefecture - filterable
- Location


# instances of each model must connect to instances of at least two other models:
Earthquakes
Location (Prefectures it hit and Resources available in the area)

Resources
Location (Earthquakes and Prefectures in the area)

Prefectures
Location (Earthquakes and Resources in the area)

# describe two types of media for instances of each model
Earthquakes
- Picture
- Map
- Chart of severity

Prefectures
- Picture
- Map

Resources
- Picture
- Graph of percent coverage


# describe three questions that your site will answer
How many earthquakes have the people of Japan suffered through?
Which region tends to be hit the hardest by earthquakes?
How much damage have those earthquakes caused?

# Git Sha
 dba16091f1b585011507c913b138b83fb55dfa2b

# Phase 1 Completion Time:
Estimated 5 hours for each member
Actual: 11 hours - Jose R Quezada
        11 hours - Mohammad S Hakim
        12 hours - Benjamin S Truong
        12 hours - Shivabalan Balathandayuthapani
        11 hours - Tarun Ajjarapu

# Phase 2 Completion Time:
Estimated 20 hours for each member
Actual: 25 hours - Jose R Quezada
        23 hours - Mohammad S Hakim
        30 hours - Benjamin S Truong
        26 hours - Shivabalan Balathandayuthapani
        28 hours - Tarun Ajjarapu

# Phase 3 Completion Time:
Estimated 10 hours for each member
Actual: 15 hours - Jose R Quezada
        13 hours - Mohammad S Hakim
        18 hours - Benjamin S Truong
        16 hours - Shivabalan Balathandayuthapani
        18 hours - Tarun Ajjarapu

# Phase 4 Completion Time:
Estimated 10 hours for each member
Actual: 10 hours - Jose R Quezada
        10 hours - Mohammad S Hakim
        10 hours - Benjamin S Truong
        10 hours - Shivabalan Balathandayuthapani
        10 hours - Tarun Ajjarapu

# Phase 1 Leader
Shivabalan Balathandayuthapani - Plan meet ups

# Phase 2 Leader
Benjamin S Truong - Sharing expertise with database and backend, keeping track of goals and progress

# Phase 3 Leader
Mohammad S Hakim - Plan meet ups, keeping track of goals and progress

# Phase 4 Leader
Jose R Quezada - Plan meet ups, keeping track of goals and progress
